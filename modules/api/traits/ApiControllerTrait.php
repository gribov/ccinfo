<?php
namespace app\modules\api\traits;

use yii\filters\ContentNegotiator;
use yii\web\Response;

trait ApiControllerTrait
{
    /**
     * @return mixed
     */
    public function behaviors()
    {
        /** @var \phprad\accessToken\TokenModule $tokenModule */
        $tokenModule = \Yii::$app->getModule('token');

        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = $tokenModule->getCorsFilterConfig();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }
}