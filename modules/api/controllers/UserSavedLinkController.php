<?php

namespace app\modules\api\controllers;

use app\models\UserSavedLink;
use app\modules\nested_sets\models\NavigationMenu;
use yii\db\ActiveQuery;
use yii\filters\auth\HttpBearerAuth;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 * @package app\modules\api\controllers
 */
class UserSavedLinkController extends BaseActiveController
{
    /**
     * @var string
     */
    public $modelClass = 'app\models\UserSavedLink';

    /**
     * @return array
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['options']
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['update'],
            $actions['view'],
            $actions['delete'],
            $actions['create']
        );
        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareListDataProvider'];

        return $actions;
    }

    /**
     * @return UserSavedLink[]
     */
    public function prepareListDataProvider()
    {
        $model = $this->modelClass;
        /** @var  $model UserSavedLink */
        return $model::findAll(['user_id' => \Yii::$app->user->id]);
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $modelClass = $this->modelClass;
        /** @var  $modelClass UserSavedLink */

        if (!$model = $modelClass::findOne(['id' => $id, 'user_id' => \Yii::$app->user->id])) {
            throw new NotFoundHttpException();
        }

        $model->delete();
        \Yii::$app->getResponse()->setStatusCode(204);
    }

    /**
     * @return UserSavedLink
     * @throws BadRequestHttpException
     */
    public function actionCreate()
    {
        $modelClass = $this->modelClass;
        /** @var  $modelClass UserSavedLink */

        $post = \Yii::$app->request->post();

        /** @var  $model UserSavedLink */
        $model = new $modelClass();
        $model->load(['UserSavedLink' => $post]);
        $model->user_id = \Yii::$app->user->identity->id;

        if (!$model->validate() || !$model->save()) {
            $msg = array_reduce($model->errors, function ($res, $errs) {
                return $res . join(';', $errs);
            }, '');

            throw new BadRequestHttpException($msg);
        }

        return $model;
    }
}
