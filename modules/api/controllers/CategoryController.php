<?php

namespace app\modules\api\controllers;

use app\modules\nested_sets\models\Category;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 * @package app\modules\api\controllers
 */
class CategoryController extends BaseRestController
{
    /**
     * @return mixed
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'home' => ['GET'],
            ],
        ];

        return $behaviors;
    }

    /**
     * /api/v1/page/home
     * @param $slug string
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionSlug($slug)
    {

        $category = Category::find()
            ->where(['slug' => $slug])
            ->with('pages')
            ->one();

        if (!$category) {
            throw new NotFoundHttpException();
        }

        if ($category->permanent_slug) {
            $category->slug = $category->permanent_slug;
            $category->permanent_slug = true;
        }

        $children = Category::arrayNestedSets(
            $category->children()
                ->select(['id', 'name', 'slug', 'permanent_slug', 'rgt', 'lft', 'depth'])
                ->with('activePages')
                ->asArray()
                ->all()
        );

        return [
            'item' => $category,
            'children' => $children,
        ];
    }
}
