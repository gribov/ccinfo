<?php

namespace app\modules\api\controllers;

use app\models\ResponsibleUser;
use app\models\UserSavedLink;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\web\BadRequestHttpException;

/**
 * Class DefaultController
 * @package app\modules\api\controllers
 */
class SearchController extends BaseRestController
{
    /**
     * @param $search
     * @return array
     */
    public function actionIndex($search)
    {
       return [
           'result_1_' . $search,
           'result_2_' . $search,
           'result_3_' . $search,
       ];
    }
}
