<?php

namespace app\modules\api\controllers;

use app\models\Competition;
use app\models\CompetitionParticipant;
use app\models\TemplateSettings;
use app\modules\nested_sets\models\NavigationMenu;
use app\modules\slider\models\Image;
use app\modules\slider\models\Slider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 * @package app\modules\api\controllers
 */
class WidgetsController extends BaseRestController
{
    /**
     * @return mixed
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
        ];

        return $behaviors;
    }

    /**
     * @param $slug
     * @return Slider|array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public function actionSlider($slug)
    {
        $slider = Slider::find()->where(['code' => $slug])->with('images')->one();
        if (!$slider) {
            throw new NotFoundHttpException('Slider does not exists!');
        }

        return $slider;
    }

    /**
     * @return array
     */
    public function actionBirthdayList()
    {
        // заглушка
        return [
            /*['name' => 'Гладкова Дарина', 'birthDate' => '22 ноября'],
            ['name' => 'Николай Андреев', 'birthDate' => '23 ноября'],*/
        ];
    }

    /**
     * @param string $position
     * @return CompetitionParticipant[]|mixed
     * @throws NotFoundHttpException
     */
    public function actionCompetition($position = 'home')
    {
        $model = Competition::find()
            ->where(['position' => $position, 'is_active' => true])
            ->with('competitionParticipants')
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('Competition does not exists!');
        }

        return [
            'items' => $model->competitionParticipants,
            'title' => $model->name
        ];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionSettings()
    {
        return [
            'background' => TemplateSettings::getTemplateSettingsBackground(),
        ];
    }
}
