<?php

namespace app\modules\api\controllers;

use app\modules\api\traits\ApiControllerTrait;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Class BaseActiveController
 * @package app\modules\api\controllers
 */
abstract class BaseActiveController extends ActiveController
{
    use ApiControllerTrait;

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $origins = \Yii::$app->getModule('token')->corsAllowOrigins;

        \Yii::$app->response->headers->set('Access-Control-Allow-Origin', rtrim(join(',', $origins), ','));
        \Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Authorization, Content-Type');
        \Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH');

        if (\Yii::$app->request->method === 'OPTIONS') {
            \Yii::$app->response->setStatusCode(200);
            return;
        }

        return parent::beforeAction($action);
    }
}
