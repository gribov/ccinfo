<?php

namespace app\modules\api\controllers;

use app\modules\admin\models\Page;
use app\modules\nested_sets\models\NavigationMenu;
use yii\helpers\ArrayHelper;

/**
 * Class DefaultController
 * @package app\modules\api\controllers
 */
class MenuController extends BaseRestController
{
    /**
     * @return mixed
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
        ];

        return $behaviors;
    }

    /**
     * /api/v1/page/home
     * @return array
     */
    public function actionIndex()
    {
        $mainMenu = NavigationMenu::findOne(['slug' => NavigationMenu::ROOT_ID]);

        $navigationMainMenu = NavigationMenu::arrayNestedSets(
            $mainMenu->children()
            ->select(['id', 'name', 'category_id', 'slug', 'permanent_slug', 'page_id', 'rgt', 'lft', 'depth'])
            ->where(['active' => NavigationMenu::STATUS_ACTIVE])
            ->with(['category', 'activePage'])
            ->asArray()
            ->all()
        );

        return array_map(function($item) {
            $item['item']['children'] = $item['children'];

            return $item['item'];
        }, $navigationMainMenu);
    }
}
