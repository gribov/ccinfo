<?php

namespace app\modules\api\controllers;

use app\models\ResponsibleUser;
use app\models\UserSavedLink;
use app\modules\nested_sets\models\NavigationMenu;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 * @package app\modules\api\controllers
 */
class ResponsibleUserController extends BaseActiveController
{
    /**
     * @var string
     */
    public $modelClass = 'app\models\ResponsibleUser';

    /**
     * @return array
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['options'],
            'optional' => ['index']
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['create'],
            'rules' => [
                [
                    'actions' => ['create'],
                    'allow' => true,
                    'roles' => ['responsible', 'admin'],
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['update'],
            $actions['view'],
            $actions['delete'],
            $actions['create'],
            $actions['index']
        );

        return $actions;
    }

    /**
     * @return ResponsibleUser
     * @throws BadRequestHttpException
     */
    public function actionCreate()
    {
        $modelClass = $this->modelClass;
        /** @var  $modelClass UserSavedLink */

        $post = \Yii::$app->request->post();

        /** @var  $model ResponsibleUser */
        $model = new $modelClass();
        $model->load(['ResponsibleUser' => $post]);
        $model->user_id = \Yii::$app->user->identity->id;

        if (!$model->validate() || !$model->save()) {
            \Yii::$app->response->setStatusCode(400);

            return $model->errors;
        }

        return $model;
    }

    /**
     * @return UserSavedLink
     */
    public function actionIndex()
    {
        $modelClass = $this->modelClass;
        /** @var  $modelClass UserSavedLink */

        return $modelClass::find()->with('user')->orderBy(['id' => SORT_DESC])->one();
    }
}
