<?php

namespace app\modules\api\controllers;

use yii\filters\auth\HttpBearerAuth;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 * @package app\modules\api\controllers
 */
class DefaultController extends BaseRestController
{
    /**
     * @return mixed
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['options'],
            'optional' => ['ver', '404']
        ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                '404' => ['POST'],
            ],
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function actionVer()
    {
        return [
            'version' => \Yii::$app->params['app_version'] ?? null
        ];
    }

    /**
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function action404()
    {
        if ($url = \Yii::$app->request->getBodyParam('url')) {
            throw new NotFoundHttpException(sprintf('Route not found : %s', $url));
        }
        throw new BadRequestHttpException();
    }

    /**
     * @return array
     */
    public function actionSecret()
    {
        return [
            'secret' => 'bar'
        ];
    }
}
