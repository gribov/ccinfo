<?php

namespace app\modules\api\controllers;

use app\modules\api\traits\ApiControllerTrait;
use yii\web\Response;
use yii\rest\Controller;

/**
 * Class BaseRestController
 * @package app\modules\api\controllers
 */
abstract class BaseRestController extends Controller
{
    use ApiControllerTrait;

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\console\Response|Response
     */
    public function beforeAction($action)
    {
        /** @var \phprad\accessToken\TokenModule $tokenModule */
        $tokenModule = \Yii::$app->getModule('token');
        $origins = implode(' ', $tokenModule->getCorsFilterConfig()['cors']['Origin']);

        // add hock
        if (\Yii::$app->request->method === 'OPTIONS') {
            \Yii::$app->response->setStatusCode(200);
            \Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Authorization, Content-Type');
            \Yii::$app->response->headers->set('Access-Control-Allow-Origin', $origins);

            return false;
        }

        return parent::beforeAction($action);
    }
}
