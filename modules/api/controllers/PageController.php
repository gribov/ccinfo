<?php

namespace app\modules\api\controllers;

use app\modules\admin\models\Page;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class DefaultController
 * @package app\modules\api\controllers
 */
class PageController extends BaseRestController
{
    /**
     * @param $slug
     * @return Page|array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public function actionSlug($slug)
    {
        $page = Page::find()->where(['slug' => $slug, 'active' => Page::STATUS_ACTIVE])->one();

        if (!$page) {
            throw new NotFoundHttpException('Page not found');
        }

        return $page;
    }
}
