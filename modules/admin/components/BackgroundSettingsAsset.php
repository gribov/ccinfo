<?php

namespace app\modules\admin\components;

use yii\web\AssetBundle;

/**
 * Class SliderAsset
 * @package app\assets
 */
class BackgroundSettingsAsset extends AssetBundle
{
    /** @var string */
    public $sourcePath = '@app/modules/admin/assets';

    /** @var array */
    public $css = [
        'background-settings.css',
    ];

    /** @var array */
    public $js = [
        'background-settings.js',
    ];

    /** @var array */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
