<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\modules\admin\models\Page;

/* @var $this yii\web\View */
/* @var $model Page */
/* @var $form yii\widgets\ActiveForm */
/* @var $dropdownRootCategories array */
/* @var $dropdownCategories array */

$this->registerJs("CKEDITOR.plugins.addExternal('collapsibleItem', '/assets-admin/ckeditor_plugins/collapsibleItem/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('bootstrapTabs', '/assets-admin/ckeditor_plugins/bootstrapTabs/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('autogrow', '/assets-admin/ckeditor_plugins/autogrow/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('lineutils', '/assets-admin/ckeditor_plugins/lineutils/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('clipboard', '/assets-admin/ckeditor_plugins/clipboard/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('widgetselection', '/assets-admin/ckeditor_plugins/widgetselection/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('widget', '/assets-admin/ckeditor_plugins/widget/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('uploadimage', '/assets-admin/ckeditor_plugins/uploadimage/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('uploadwidget', '/assets-admin/ckeditor_plugins/uploadwidget/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('notification', '/assets-admin/ckeditor_plugins/notification/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('filetools', '/assets-admin/ckeditor_plugins/filetools/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('notificationaggregator', '/assets-admin/ckeditor_plugins/notificationaggregator/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('filebrowser', '/assets-admin/ckeditor_plugins/filebrowser/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('popup', '/assets-admin/ckeditor_plugins/popup/plugin.js', '');");
$this->registerJs("CKEDITOR.plugins.addExternal('uploadfile', '/assets-admin/ckeditor_plugins/uploadfile/plugin.js', '');");
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-12">
            <?= $form->field($model, 'active')->radioList([0 => 'Не опубликовано', 1 => 'Опубликовано'],
                [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return
                            Html::radio($name, $checked, ['id' => Page::tableName().'-'.$index, 'value' => $value]) . '<label for="'.Page::tableName().'-'.$index.'">' . $label . '</label>';
                    },
            ]) ?>
        </div>

        <div class="col-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'root_category_id')->dropDownList($dropdownRootCategories,
                [
                    'prompt' => 'Выберите раздел',
                ])
            ?>
        </div>

        <div class="col">
            <?= $form->field($model, 'category_id')->dropDownList($model->isNewRecord ? [] : $dropdownCategories, [
                'prompt' => 'Выберите категорию',
                'disabled' => $model->isNewRecord ? true : false
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <?= $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(), [
                'options' => ['rows' => 12],
                'preset' => 'custom',
                'clientOptions' => [
                    'language' => 'ru',
                    'extraPlugins' => 'collapsibleItem,lineutils,clipboard,widgetselection,widget,autogrow,bootstrapTabs,filebrowser,popup,uploadfile,uploadimage,uploadwidget,notification,filetools,notificationaggregator',
                    'filebrowserUploadUrl' => 'image-uploader',
                    'allowedContent' => true,
                    'customConfig' => '',
                    'autoGrow_minHeight' => 400,
                    'autoGrow_maxHeight' => 600,
                    'toolbar' => [
                        ['Source'],
                        ['-'],
                        ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'],
                        ['-'],
                        ['TextColor', 'BGColor'],
                        ['-'],
                        ['CopyFormatting', 'RemoveFormat'],
                        ['-'],
                        ['NumberedList', 'BulletedList'],
                        ['-'],
                        ['Outdent', 'Indent'],
                        ['-'],
                        ['Blockquote', 'CreateDiv'],
                        ['-'],
                        ['JustifyLeft', 'JustifyRight', 'JustifyCenter', 'JustifyBlock'],
                        ['-'],
                        ['Link', 'Unlink', 'Anchor'],
                        ['-'],
                        ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
                        ['-'],
                        ['CollapsibleItem', 'BootstrapTabs'],
                        ['-'],
                        ['Maximize', 'ShowBlocks', 'Styles', 'Format', 'Font', 'FontSize'],
                    ]
                ]
            ]) ?>
        </div>


        <div class="form-group col-12">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php \app\widgets\Script::begin() ?>
<script>
    var categoriesDropdown = $('#page-category_id');

    $(document).on('change', '#page-root_category_id', function () {
        if($(this).val()) {

            categoriesDropdown.html('');

            var thisEl = $(this);
            thisEl.prop('disabled', true);
            $.ajax({
                type: 'GET',
                url: '<?=\yii\helpers\Url::toRoute(['/admin/ajax/get-categories'])?>',
                data: {
                    id: thisEl.val(),
                }
            }).done(function (data) {
                if (data) {

                    for (var i = 0; i < data.length; i++) {
                        categoriesDropdown.append('<option value="' + data[i]['id'] + '">' + data[i]['name'] + '</option>');
                    }

                    categoriesDropdown.prop('disabled', false);
                    thisEl.prop('disabled', false);
                }
            })
        }
    });
</script>
<?php \app\widgets\Script::end() ?>
