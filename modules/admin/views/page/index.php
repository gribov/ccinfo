<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            [
                'attribute' => 'active',
                'content' => function ($model) {
                  return $model->active ? 'Да' : 'Нет';
                }
            ],
            'created_at:datetime',
            ['attribute' => 'creator.name', 'label' => 'Создал'],
            'updated_at:datetime',
            ['attribute' => 'updator.name', 'label' => 'Обновил'],
            ['class' => 'app\components\MaterialActionColumn'],
        ],
    ]); ?>
</div>
