<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \app\modules\admin\models\Page */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'content:raw',
            [
                'attribute' => 'category_id',
                'value' => function(\app\modules\admin\models\Page $model) {
                    return $model->category ? $model->category->name : '(Не задано)';
                }
            ],
            'slug',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
