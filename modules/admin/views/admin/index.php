<?php

use yii\helpers\Url;

?>

<div>
  <?php if(Yii::$app->user->identity): ?>
  <p>Привет дружище : <b><?= Yii::$app->user->identity->username ?></b></p>
  <p>Роли : <b><?= Yii::$app->user->identity->rolesAsString ?></b></p>
  <?php endif ?>
  <p><a href="<?= Url::toRoute('logout')?>"> Выйти</a></p>
</div>
