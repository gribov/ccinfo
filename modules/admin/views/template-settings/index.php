<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

  <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
      <?= Html::a('Создать настройку', ['create'], ['class' => 'btn btn-success']) ?>
  </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'class' => 'app\components\TemplateSettingsColumn',
                'label' => 'Значение',
            ],
            ['attribute' => 'user.name', 'label' => 'Обновил'],
            [
                'class' => 'app\components\MaterialActionColumn',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $route = \yii\helpers\Url::toRoute(['/admin/template-settings/update', 'id' => $model->id]);
                        if ($model->code === 'background') {
                            $route = \yii\helpers\Url::toRoute('/admin/template-background/update');
                        }
                        return Html::a('<span class="ti-pencil"></span>', $route);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
