<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Competition */

/* @var $form yii\widgets\ActiveForm */

use \app\models\Competition;

?>

<div class="competition-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'position')->dropDownList(
        Competition::getPositions(),
        ['prompt' => 'Выберите позитцию']
    ) ?>

    <?= $form->field($model, 'is_active')->radioList([1 => 'Активен', 0 => 'Не активен'],
        [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio(
                        $name,
                        $checked,
                        ['id' => Competition::tableName() . '-' . $index, 'value' => $value]
                    )
                    . '<label for="' . Competition::tableName() . '-' . $index . '">' . $label . '</label><br>';
            },
        ]) ?>

  <div class="form-group">
      <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
