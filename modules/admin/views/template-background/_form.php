<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\TemplateBackgroundForm;
use app\modules\admin\components\BackgroundSettingsAsset;

/* @var $this yii\web\View */
/* @var $model \app\models\TemplateBackgroundForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $existingImages array */

BackgroundSettingsAsset::register($this);

?>

<div class="background-change-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'type')->dropDownList(TemplateBackgroundForm::getBackgroundTypes())->label('Тип заднего фона') ?>

  <div class="background-image-settings" style="display: none;">
      <?= $form->field($model, 'imageFile')->fileInput(['accept' => 'image/*'])->label('') ?>

      <?= $form->field($model, 'repeatImage')->dropDownList(['0' => 'Растянуть', '1' => 'Замостить'])->label('Тип заднего фона') ?>

      <?= $form->field($model, 'imageFileName')->hiddenInput()->label('') ?>

    <div class="form-group">
        <?php if ($model->imageFileAttached) : ?>
          <div class="using-image">
            <h4>Используемое изображение</h4>
            <img src="<?= $model->imageFileSrc ?>"/>
          </div>
          <hr>
        <?php endif ?>
    </div>

    <?php if ($existingImages) : ?>
      <h4>Существующие изображения </h4>
      <ul id="images-list">
          <?php foreach ($existingImages as $image) : ?>
            <li>
              <a href="" class="image-delete" data-name="<?= $image['name'] ?>"></a>
              <div class="preview-image" data-name="<?= $image['name'] ?>">
                <img src="<?= $image['src'] ?>"/>
              </div>
            </li>
          <?php endforeach ?>
      </ul>
    <?php endif ?>

  </div>

  <div class="background-color-settings" style="display: none;">
      <?= $form->field($model, 'color')->input('text')->label('Цвет') ?>
  </div>

  <div class="form-group">
      <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
  </div>


    <?php ActiveForm::end(); ?>
</div>
