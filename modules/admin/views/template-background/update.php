<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\TemplateBackgroundForm */
/* @var $entity \app\models\TemplateSettings */
/* @var $existingImages array */

$this->title = 'Изменить настройку ' . $entity->name;
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['/admin/template-settings/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'existingImages' => $existingImages,
    ]) ?>

</div>
