<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CompetitionParticipant */
/* @var $competition app\models\Competition */

$this->title = 'Просмотр участника ' .  $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['/admin/competition/index']];
$this->params['breadcrumbs'][] = ['label' => $competition->name, 'url' => ['index', 'competition_id' => $competition->id]];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="competition-participant-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        &nbsp;
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

  <div class="row">
    <div class="col-md-8">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                'competition.name',
            ],
        ]) ?>
    </div>
    <div class="col-md-4">
      <div>
        <img src="<?= $model->photoSrc; ?>">
      </div>

    </div>

  </div>

  


</div>
