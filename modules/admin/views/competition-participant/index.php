<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $competition \app\models\Competition */

$this->title = 'Участники';
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['/admin/competition/index']];
$this->params['breadcrumbs'][] = $competition->name;
?>
<div class="competition-participant-index">

  <h1><?= Html::encode($this->title) ?></h1>

  <?php if($competition->canAddParticipant()): ?>
    <p>
        <?= Html::a('Добавить', ['create', 'competition_id' => $competition->id], ['class' => 'btn btn-success']) ?>
    </p>
  <?php else: ?>
    <p>Можно добавить не больше 3 победителей.</p>
  <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'class' => 'yii\grid\DataColumn',
                'content' => function (\app\models\CompetitionParticipant $model) {
                    return '<img height="100px" src="' . $model->photoSrc . '">';
                },
                'header' => 'Фото'
            ],
            'competition.name',
            ['class' => 'app\components\MaterialActionColumn'],
        ],
    ]); ?>
</div>
