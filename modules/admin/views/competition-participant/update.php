<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CompetitionParticipant */


$this->title = 'Изменение участника ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['/admin/competition/index']];
$this->params['breadcrumbs'][] = ['label' => $competition->name, 'url' => ['index', 'competition_id' => $competition->id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="competition-participant-update">

  <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
