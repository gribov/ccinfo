<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Page;
use app\modules\nested_sets\models\Category;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class AjaxController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        if(!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException();
        }

        return parent::beforeAction($action);
    }

    /**
     * Отдает дочерние категории по id корневой категории
     * @param $id
     * @return bool|null|static
     */
    public function actionGetCategories($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(!$id) {
            return false;
        }

        $categories = Category::findOne($id);
        $categories = $categories->children()->asArray()->all();

        return $categories;
    }

    /**
     * Автодополнение категории в форме главного меню
     * @param $q
     * @param $filter
     * @return array
     */
    public function actionAutocompleteCategory($q, $filter = false)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        $idColumn = 'id';

        if(filter_var($filter, FILTER_VALIDATE_BOOLEAN)) {
            $idColumn = 'name as id';
        }

        $categories = Category::find()
            ->select([$idColumn, 'name as text'])
            ->where(['ilike', 'name', $q])
            ->limit(10)
            ->asArray()
            ->all();

        $out['results'] = $categories;

        return $out;
    }

    /**
     * Автодополнение страницы в форме главного меню
     * @param $q
     * @param $filter
     * @return array
     */
    public function actionAutocompletePage($q, $filter = false)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        $idColumn = 'id';

        if(filter_var($filter, FILTER_VALIDATE_BOOLEAN)) {
            $idColumn = 'title as id';
        }

        $categories = Page::find()
            ->select([$idColumn, 'title as text'])
            ->where(['ilike', 'title', $q])
            ->limit(10)
            ->asArray()
            ->all();

        $out['results'] = $categories;

        return $out;
    }

}
