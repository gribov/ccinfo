<?php

namespace app\modules\admin\controllers;

use app\models\TemplateSettings;
use app\models\UploadedFileForValidate;
use app\modules\nested_sets\models\Category;
use Yii;
use app\modules\admin\models\Page;
use app\modules\admin\models\search\PageSearch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TemplateSettingsController implements the CRUD actions for Page model.
 */
class TemplateSettingsController extends Controller
{
    public $layout = 'admin';

    public $enableCsrfValidation = false;

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => TemplateSettings::find()->all()
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
