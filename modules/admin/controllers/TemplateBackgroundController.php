<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\TemplateBackgroundForm;
use app\models\TemplateSettings;
use yii\filters\VerbFilter;
use \yii\helpers\FileHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * TemplateSettingsController implements the CRUD actions for Page model.
 */
class TemplateBackgroundController extends Controller
{
    public $layout = 'admin';

    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-image' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * @return string|\yii\web\Response
     */
    public function actionUpdate()
    {
        $entity = $this->findModel();
        $model = new TemplateBackgroundForm();
        $model->loadEntity($entity);
        $usedFileName = $model->imageFileName;

        $files = array_filter($this->getExistingImages(), function($file) use ($usedFileName) {
            return $file['name'] !== $usedFileName;
        });

        if (Yii::$app->request->isPost
            && $model->load(Yii::$app->request->post())
            && $model->identifyScenario()
        ) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            if ($model->validate() && $model->saveImage() && $model->saveEntity($entity)) {
                return $this->redirect(['/admin/template-settings/index']);
            }
        }
        $model->scenario = 'default';

        return $this->render('update', [
            'model' => $model,
            'entity' => $entity,
            'existingImages' => $files
        ]);
    }

    public function actionDeleteImage()
    {
        $image = Yii::$app->request->post('image');
        if (!$image) {
            throw new BadRequestHttpException();
        }

        $path = \Yii::getAlias('@webroot/images/background_images/') . $image;
        if (file_exists($path)) {
            unlink($path);
        }
    }

    /**
     * @return null|TemplateSettings
     * @throws NotFoundHttpException
     */
    protected function findModel()
    {
        if (($model = TemplateSettings::findOne(['code' => 'background'])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return array
     */
    protected function getExistingImages()
    {
        $files = FileHelper::findFiles(
            \Yii::getAlias('@webroot/images/background_images/')
            , ['only' => ['*.pjpeg', '*.jpeg', '*.jpg', '*.png']]
        );

        return array_map(function($file) {
            $pathInfo = pathinfo($file);
            return [
                'name' => $pathInfo['filename'] . '.' . $pathInfo['extension'],
                'src' => \Yii::getAlias('@web/images/background_images/') . $pathInfo['filename'] . '.' . $pathInfo['extension']
            ];
        }, $files);
    }
}
