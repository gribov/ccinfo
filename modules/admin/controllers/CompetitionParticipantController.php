<?php

namespace app\modules\admin\controllers;

use app\models\Competition;
use Yii;
use app\models\CompetitionParticipant;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CompetitionParticipantController implements the CRUD actions for CompetitionParticipant model.
 */
class CompetitionParticipantController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'admin';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $competition_id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($competition_id)
    {
        $competition = Competition::findOne(['id' => $competition_id]);

        if (!$competition) {
            throw new NotFoundHttpException();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => CompetitionParticipant::find()
                ->where(['competition_id' => $competition_id])
                ->with('competition'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'competition' => $competition,
        ]);
    }

    /**
     * Displays a single CompetitionParticipant model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'competition' => $model->competition,
        ]);
    }

    /**
     * @param $competition_id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreate($competition_id)
    {
        $competition = Competition::findOne(['id' => $competition_id]);

        if (!$competition) {
            throw new NotFoundHttpException();
        }

        if (!$competition->canAddParticipant()) {
            return $this->redirect([
                'index',
                'competition_id' => $competition->id
            ]);
        }

        $model = new CompetitionParticipant(['competition_id' => $competition_id]);

        if ($model->load(Yii::$app->request->post())) {
            $model->photoFile = UploadedFile::getInstance($model, 'photoFile');
            if ($model->upload() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'competition' => $competition,
        ]);
    }

    /**
     * Updates an existing CompetitionParticipant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->photoFile = UploadedFile::getInstance($model, 'photoFile');
            if ($model->photoFile) {
                $model->upload();
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'competition' => $model->competition,
        ]);
    }

    /**
     * Deletes an existing CompetitionParticipant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'competition_id' => $model->competition->id]);
    }

    /**
     * Finds the CompetitionParticipant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompetitionParticipant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompetitionParticipant::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
