<?php

namespace app\modules\admin\controllers;

use app\models\UploadedFileForValidate;
use app\modules\nested_sets\models\Category;
use Yii;
use app\modules\admin\models\Page;
use app\modules\admin\models\search\PageSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    public $layout = 'admin';
    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page(['active' => 0]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $dropdownRootCategories = ArrayHelper::map(Category::find()->where(['depth' => 0])->all(), 'id', 'name');

        return $this->render('create', [
            'model' => $model,
            'dropdownRootCategories' => $dropdownRootCategories
        ]);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $dropdownRootCategories = ArrayHelper::map(Category::find()->where(['depth' => 0])->all(), 'id', 'name');
        $dropdownCategories = ArrayHelper::map($model->rootCategory->children()->all(), 'id', 'name');

        return $this->render('update', [
            'model' => $model,
            'dropdownRootCategories' => $dropdownRootCategories,
            'dropdownCategories' => $dropdownCategories
        ]);
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Загрузка картинки через кнопку визуального редактора
     * @return bool|string
     */
    public function actionImageUploader()
    {
        $file = UploadedFile::getInstanceByName('upload');

        if ($file) {
            $fileModel = new UploadedFileForValidate(['scenario' => UploadedFileForValidate::SCENARIO_IMAGE]);
            $fileModel->imageFile = $file;

            $commonUrl = '/assets-admin/ckeditor_uploaded_images/' . time() . '_' . Yii::$app->security->generateRandomString(8) . '.' . $fileModel->imageFile->extension;
            $urlToRender = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . ($_SERVER['SERVER_PORT'] ? ':' . $_SERVER['SERVER_PORT'] : '') . $commonUrl;
            $funcNum = Yii::$app->request->get('CKEditorFuncNum');

            if ($fileModel->validate()) {
                $urlToSave = Yii::getAlias('@webroot') . $commonUrl;

                if($fileModel->imageFile->saveAs($urlToSave)) {
                    return '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' . $funcNum . '", "' . $urlToRender . '");</script>';
                }

            } else {
               return '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' . $funcNum . '", "' . $urlToRender . '", "' . $fileModel->getFirstError('imageFile') . '");</script>';
            }
        }

        return false;
    }

    /**
     * Загрузка файла через кнопку drag and drop
     * @return string
     */
    public function actionFileUploader()
    {
        $file = UploadedFile::getInstanceByName('upload');

        if ($file) {
            $fileModel = new UploadedFileForValidate(['scenario' => UploadedFileForValidate::SCENARIO_FILE]);
            $fileModel->anotherFile = $file;

            $commonUrl = '/assets-admin/ckeditor_uploaded_files/' . time() . '_' . Yii::$app->security->generateRandomString(8) . '.' . $fileModel->anotherFile->extension;
            $urlToRender = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . ($_SERVER['SERVER_PORT'] ? ':' . $_SERVER['SERVER_PORT'] : '') . $commonUrl;
            $funcNum = Yii::$app->request->get('CKEditorFuncNum');

            if ($fileModel->validate()) {
                $urlToSave = Yii::getAlias('@webroot') . $commonUrl;

                if($fileModel->anotherFile->saveAs($urlToSave)) {
                    return json_encode([
                        'uploaded' => 1,
                        'fileName' => $fileModel->anotherFile->name,
                        'url' => $urlToRender
                    ]);
                }

            } else {
                return json_encode([
                    'uploaded' => 0,
                    'error' => [
                        'message' => $fileModel->getFirstError('anotherFile')
                    ],
                ]);
            }
        }

        return false;
    }
}
