$(function($) {
  'use strict';

  var TYPE_BACKGROUND_IMAGE = 2,
    TYPE_BACKGROUND_COLOR = 1;

  var usingImageName = $('#templatebackgroundform-imagefilename').val();

  showBlocksByType($('#templatebackgroundform-type').val());

  $('#templatebackgroundform-type').change(function() {
    showBlocksByType(this.value);
  });

  $('#images-list .preview-image').click(function() {
    if($(this).hasClass('selected')) {
      $(this).removeClass('selected');
      $('#templatebackgroundform-imagefilename').val(usingImageName);
      return;
    }
    $('#images-list .preview-image.selected').removeClass('selected');
    $(this).addClass('selected');
    $('#templatebackgroundform-imagefilename').val($(this).attr('data-name'));
  });

  $('.image-delete').click(function(e) {
    e.preventDefault();
    if (confirm('Удалить файл с сервера?')) {
      var that = this,
        imageName = this.getAttribute('data-name');
      $.post('/admin/template-background/delete-image', {image: imageName}, function() {
        $(that).parent('li').remove();
      });
    }
  });


  function showBlocksByType(value) {
    value = +value;
    if (value === TYPE_BACKGROUND_COLOR) {
      $('.background-image-settings').hide();
      $('.background-color-settings').show();
    } else {
      $('.background-image-settings').show();
      $('.background-color-settings').hide();
    }
  }
});