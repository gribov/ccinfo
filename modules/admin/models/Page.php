<?php

namespace app\modules\admin\models;

use app\models\User;
use app\modules\nested_sets\models\Category;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property integer $active
 * @property string $title
 * @property string $content
 * @property int $root_category_id
 * @property int $category_id
 * @property string $slug
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Category $rootCategory
 * @property Category $category
 */
class Page extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title'
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['category_id', 'root_category_id'], 'default', 'value' => 0],
            [['active', 'category_id', 'root_category_id'], 'integer'],
            ['active', 'default', 'value' => 0],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Публикация',
            'title' => 'Заголовок',
            'content' => 'Контент',
            'category_id' => 'Принадлежит категории',
            'root_category_id' => 'Принадлежит разделу',
            'slug' => 'Slug',
            'created_at' => 'Создана',
            'updated_at' => 'Обновлена',
            'created_by' => 'Создан',
            'updated_by' => 'Обновлен',
        ];
    }

    /**
     * Связанная корневая категория
     * @return \yii\db\ActiveQuery
     */
    public function getRootCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'root_category_id']);
    }

    /**
     * Категория, которой принадлежит страница
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdator()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
