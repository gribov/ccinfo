<?php

namespace app\modules\slider\models;

use Yii;

/**
 * This is the model class for table "phprad_slider".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $type
 *
 * @property Image[] $images
 */
class Slider extends \yii\db\ActiveRecord
{
    /** default slider type name */
    const DEFAULT_SLIDER_TYPE_NAME = 'Стандартный';

    /** default slider type */
    const DEFAULT_SLIDER_TYPE_CODE = 1;

    /** types */
    const SLIDER_TYPES = [
        self::DEFAULT_SLIDER_TYPE_CODE => self::DEFAULT_SLIDER_TYPE_NAME
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'phprad_slider';
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'name',
            'code',
            'type',
            'images' => function ($model) {
                return $model->images;
            }
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['code'], 'string', 'max' => 50],
            [['type'], 'number'],
            [['type'], 'default', 'value' => self::DEFAULT_SLIDER_TYPE_CODE],
            [['type'], 'in', 'range' => array_keys(self::SLIDER_TYPES)],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'code' => 'Код (латиницей)',
            'type' => 'Тип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['slider_id' => 'id']);
    }
}
