<?php

namespace app\modules\slider\models;

use  \yii\base\Model;
use yii\web\UploadedFile;

class SliderImageFiles extends Model
{
    /** @var UploadedFile[] */
    public $files = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['files'],
                'file',
                'skipOnEmpty' => true,
                'extensions' => ['png', 'jpg', 'jpeg', 'gif'],
                'checkExtensionByMimeType'=>false,
                'maxFiles' => 10
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'files' => ''
        ];
    }

    /**
     * @return array|bool
     */
    public function upload()
    {
        if (!$this->validate()) {
            return false;
        }
        $res = [];
        foreach ($this->files as $file) {
            $filename = md5(time()) . '_' . $file->baseName . '.' . $file->extension;
            $savePath = \Yii::getAlias('@slider_dir') . '/' . $filename;
            if ($file->saveAs($savePath)) {
                $res[] = $filename;
            }
        }

        return $res;
    }
}