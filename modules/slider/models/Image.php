<?php

namespace app\modules\slider\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "phprad_slider_image".
 *
 * @property int $id
 * @property string $title
 * @property string $file_name
 * @property int $slider_id
 *
 * @property Slider $slider
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'phprad_slider_image';
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'title',
            'src' => function ($model) {
                return Url::base(true) . $model->src;
            },
            'link',
            'link_text',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_name'], 'required'],
            [['slider_id'], 'default', 'value' => null],
            [['slider_id'], 'integer'],
            [['title', 'file_name'], 'string', 'max' => 100],
            [['link', 'link_text'], 'string', 'max' => 1000],
            [['slider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Slider::className(), 'targetAttribute' => ['slider_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'link' => 'Ссылка',
            'link_text' => 'Текст ссылки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider()
    {
        return $this->hasOne(Slider::className(), ['id' => 'slider_id']);
    }

    /**
     * @return bool|string
     */
    public function getSrc()
    {
        return Yii::getAlias('@slider_public_dir') . '/' . $this->file_name;
    }

    /**
     * @param $fileNames
     * @param Slider $slider
     * @return bool[]
     */
    public static function createFromFiles($fileNames, Slider $slider)
    {
        return array_map(function($fileName) use($slider) {
            $image = new Image([
                'file_name' => $fileName,
                'slider_id' => $slider->id
            ]);

            return $image->save();
        }, $fileNames);
    }

    /**
     * @param array $ids
     * @return array|bool
     */
    public static function deleteFiles(array $ids = [])
    {
        if (!$ids) {
            return true;
        }
        $images = self::findAll(array_unique($ids));
        foreach ($images as $image) {
            @unlink(Yii::getAlias('@slider_dir') . '/' . $image->file_name);
        }

        return self::deleteAll(['id' => $ids]);
    }

    /**
     * @param $links
     */
    public static function addLinksToEntities($links)
    {
        $data = [];
        foreach ($links as $imageId => $link) {
            $data[$imageId] = [
                'id' => $imageId,
                'link' => $link['imageLink'],
                'linkText' => $link['imageText'],
            ];
        }
        $images = Image::findAll(['id' => array_keys($data)]);

        foreach ($images as $image) {
            $image->link = $data[$image->id]['link'];
            $image->link_text = $data[$image->id]['linkText'];
            $image->save();
        }
    }
}
