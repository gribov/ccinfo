jQuery(function ($) {
  /** прикрепление файла к форме */
  $('#sliderimagefiles-files').change(function() {
    $('div[data-image-dynamic]').remove();

    $.each(this.files, function(i, file) {
      var reader  = new FileReader();
      reader.onloadend = function () {
        //preview.src = reader.result;
        var block = $('<div>');
        block.attr('class', 'preview-image');
        block.attr('data-image-dynamic', 1);

        var image = $('<img>');
        image.attr('src', reader.result);
        block.append($('<a class="image-delete" href="">Удалить</a>'));
        block.append(image);
        $('#images-list').append(block);
      };
      reader.readAsDataURL(file);
    });
  });

  /** Удаление элементов */
  $(document).on('click', '.image-delete', function(e) {
    e.preventDefault();
    var $this = $(this);
    if (!$this.attr('data-id') && confirm('Открепить изображения?')) {
      $('div[data-image-dynamic]').remove();
      $('#sliderimagefiles-files').val('');
      return;
    }
    if (!confirm('Удалить ?')) {
      return;
    }
    var value = $('.deletedIds').val();
    value = value === "" ? [] : value.split(',');
    value.push($this.attr('data-id'));
    value = value.filter(function(el, index, arr) {
      return index == arr.indexOf(el);
    });

    $('.deletedIds').val(value.join(','));

    $this.parents('.slider-image-block').remove();
  });
});