<?php

namespace app\modules\slider;

use yii\web\AssetBundle;

/**
 * Class SliderAsset
 * @package app\assets
 */
class SliderAsset extends AssetBundle
{
    /** @var string  */
    public $sourcePath = '@app/modules/slider/assets';

    /** @var array  */
    public $css = [
        'slider-image.css',
    ];

    /** @var array  */
    public $js = [
        'slider-image.js',
    ];

    /** @var array  */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
