<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\slider\SliderAsset;

SliderAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\slider\models\Slider */
/* @var $images app\modules\slider\models\Image[] */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    &nbsp;
    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Удалить?',
            'method' => 'post',
        ],
    ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
        ],
    ]) ?>

    <div class="form-group" id="images-list">
        <?php foreach ($images as $image) :?>
          <div class="preview-image">
            <img src="<?=$image->src?>"/>
          </div>
        <?php endforeach ?>
    </div>

</div>
