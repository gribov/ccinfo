<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $slider app\modules\slider\models\Slider */
/* @var $images app\modules\slider\models\Image[] */
/* @var $files app\modules\slider\models\SliderImageFiles */
/* @var $deleteImages \yii\base\DynamicModel */

$this->title = 'Изменить слайдер ' . $slider->name;
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $slider->name, 'url' => ['view', 'id' => $slider->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="slider-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'slider' => $slider,
        'images' => $images,
        'files' => $files,
        'deleteImages' => $deleteImages
    ]) ?>

</div>
