<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $slider app\modules\slider\models\Slider */
/* @var $images app\modules\slider\models\Image[] */
/* @var $files app\modules\slider\models\SliderImageFiles */

$this->title = 'Создать слайдер';
$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'slider' => $slider,
        'images' => $images,
        'files' => $files,
    ]) ?>

</div>
