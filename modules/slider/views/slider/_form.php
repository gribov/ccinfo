<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\slider\SliderAsset;

SliderAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\slider\models\Slider */
/* @var $form yii\widgets\ActiveForm */
/* @var $slider app\modules\slider\models\Slider */
/* @var $images app\modules\slider\models\Image[] */
/* @var $files app\modules\slider\models\SliderImageFiles */
/* @var $deleteImages \yii\base\DynamicModel|null */

$deleteImages = empty($deleteImages) ? null : $deleteImages;
?>


<div class="slider-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($slider, 'id')->hiddenInput()->label('')?>

    <?= $form->field($slider, 'name')->textInput(['maxlength' => true])?>

    <?= $form->field($slider, 'code')->textInput(['maxlength' => true])?>

    <?= $form->field($files, 'files[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <div class="form-group" id="images-list">
      <?php foreach ($images as $image) :?>
        <div class="slider-image-block row">
          <div class="preview-image col-md-2">
            <a href="" class="image-delete" data-id="<?= $image->id ?>">Удалить</a>
            <img src="<?=$image->src?>" />
          </div>
          <div class="slider-image-block__link-block col-md-9">
            <div class="form-group">
                <?= Html::label('Ссылка') ?>
                <?= Html::input('text', "links[$image->id][imageLink]", $image->link, ['class' => 'form-control'])?>
            </div>
            <div class="form-group">
                <?= Html::label('Текст ссылки') ?>
                <?= Html::input('text', "links[$image->id][imageText]", $image->link_text, ['class' => 'form-control'])?>
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>

    <?php if($deleteImages): ?>
      <?= $form->field($deleteImages, 'ids')->hiddenInput(['class' => 'deletedIds'])->label('')?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
