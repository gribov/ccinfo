<?php

namespace app\modules\slider\controllers;

use app\modules\slider\models\Image;
use app\modules\slider\models\SliderImageFile;
use app\modules\slider\models\SliderImageFiles;
use Yii;
use app\modules\slider\models\Slider;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    /** @var string  */
    public $layout = '@app/modules/admin/views/layouts/admin';

    /** @var string  */
    public $viewPath = '@app/modules/slider/views/default';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Slider::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'images' => Image::findAll(['slider_id' => $id])
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $slider = new Slider();
        $images = [];
        $filesWrap = new SliderImageFiles();

        if (
            Yii::$app->request->isPost &&
            $slider->load(Yii::$app->request->post()) &&
            $slider->save()
        ) {
            $filesWrap->files = UploadedFile::getInstances($filesWrap, 'files');
            /* */
            if ($fileNames = $filesWrap->upload()) {
                Image::createFromFiles($fileNames, $slider);
            }

            return $this->redirect(['view', 'id' => $slider->id]);
        }

        return $this->render('create', [
            'slider' => $slider,
            'images' => $images,
            'files' => $filesWrap
        ]);
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $slider = $this->findModel($id);
        $images = Image::findAll(['slider_id' => $id]);
        $filesWrap = new SliderImageFiles();
        $deleteImages = new DynamicModel(['ids']);
        $deleteImages->addRule(['ids'], 'string');

        if (
            Yii::$app->request->isPost &&
            $slider->load(Yii::$app->request->post()) &&
            $deleteImages->load(Yii::$app->request->post()) &&
            $slider->save()
        ) {
            if ($deleteImages->ids) {
                // delete files here
                Image::deleteFiles(array_map('intval', explode(',', $deleteImages->ids)));
            }

            $post = Yii::$app->request->post();

            if (isset($post['links'])) {
                Image::addLinksToEntities($post['links']);
            }

            $filesWrap->files = UploadedFile::getInstances($filesWrap, 'files');
            /* add new files */
            if ($fileNames = $filesWrap->upload()) {
                Image::createFromFiles($fileNames, $slider);
            }

            return $this->redirect(['view', 'id' => $slider->id]);
        }

        return $this->render('update', [
            'slider' => $slider,
            'images' => $images,
            'files' => $filesWrap,
            'deleteImages' => $deleteImages
        ]);
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
