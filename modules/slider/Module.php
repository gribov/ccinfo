<?php

namespace app\modules\slider;
use yii\base\BootstrapInterface;

/**
 * slider module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\slider\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        \Yii::setAlias('@slider_dir', '@webroot/images/slider');
        \Yii::setAlias('@slider_public_dir', '@web/images/slider');

        if (!is_dir(\Yii::getAlias('@slider_dir'))) {
            mkdir(\Yii::getAlias('@slider_dir'));
        }
    }
}
