<?php

use yii\db\Migration;

/**
 * Class m180222_135013_pre_main_menu
 */
class m180222_135013_pre_main_menu extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $menu = new \app\modules\nested_sets\models\NavigationMenu(['name' => 'Главное меню']);
        $menu->makeRoot();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $menu = \app\modules\nested_sets\models\NavigationMenu::deleteAll(['name' => 'Главное меню']);

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180222_135013_pre_main_menu cannot be reverted.\n";

        return false;
    }
    */
}
