<?php

namespace app\modules\nested_sets;

/**
 * nested_sets module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\nested_sets\controllers';
    public $defaultRoute = 'menu/index';
    public $controllerMap = 'app\modules\nested_sets\controllers\CategoryRootController';


    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
