<?php

namespace app\modules\nested_sets\controllers;

use app\modules\nested_sets\models\Category;
use Yii;
use app\modules\nested_sets\models\NavigationMenu;
use app\modules\nested_sets\models\search\NavigationMenuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * NavigationMenuController implements the CRUD actions for NavigationMenu model.
 */
class NavigationMenuController extends Controller
{
    /**
     * @var NavigationMenu $rootMenu
     */
    public $rootMenu;

    /**
     * @var string
     */
    public $defaultAction = 'index';

    /**
     * @var string
     */
    public $layout = '@app/modules/admin/views/layouts/admin';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->rootMenu = NavigationMenu::findOne(['slug' => NavigationMenu::ROOT_ID]);
        if (!$this->rootMenu) {
            throw new NotFoundHttpException('Root menu not found');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NavigationMenu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NavigationMenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NavigationMenu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NavigationMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NavigationMenu(['active' => 0]);

        if ($model->load(Yii::$app->request->post())) {

            if ($parent = Yii::$app->request->post('parent')) {
                $parent = NavigationMenu::findOne(['id' => (int)$parent]);
                $model->prependTo($parent);
            } else {
                $model->prependTo($this->rootMenu);
            }

            if ($insertAfter = Yii::$app->request->post('insert_after')) {
                $insertAfter = NavigationMenu::findOne(['id' => (int)$insertAfter]);

                $model->insertAfter($insertAfter);
            }

            Yii::$app->session->setFlash('success', 'Запись успешно сохранена!');

            return $this->redirect(['update', 'id' => $model->id]);
        }

        $dropDownMenuAr = ArrayHelper::map($this->rootMenu->children()->where(['!=', 'depth', 0])->all(), 'id', 'name');

        return $this->render('create', [
            'model' => $model,
            'dropdownMenuAr' => $dropDownMenuAr,
            'modelParent' => false,
            'modelBefore' => false,
        ]);
    }

    /**
     * Updates an existing NavigationMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {

            if ($parent = Yii::$app->request->post('parent')) {
                $parent = NavigationMenu::findOne(['id' => (int) $parent]);
                $model->prependTo($parent);
            }

            if ($insertAfter = Yii::$app->request->post('insert_after')) {
                $insertAfter = NavigationMenu::findOne(['id' => (int) $insertAfter]);
                $model->insertAfter($insertAfter);
            }

            Yii::$app->session->setFlash('success', 'Запись успешно сохранена!');

            return $this->redirect(['update', 'id' => $model->id ]);
        }

        $dropdownMenuAr = ArrayHelper::map($this->rootMenu->children()->where(['!=', 'id', $model->id])->andWhere(['!=', 'depth', 0])->all(), 'id', 'name');

        $modelParent = $model->parents(1)->one();

        $modelBefore = $model->prev()->one();

        return $this->render('update', [
            'model' => $model,
            'dropdownMenuAr' => $dropdownMenuAr,
            'modelParent' => $modelParent,
            'modelBefore' => $modelBefore,
        ]);
    }

    /**
     * Deletes an existing NavigationMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NavigationMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NavigationMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NavigationMenu::find()->where(['id' => $id])->with(['category', 'page'])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
