<?php

namespace app\modules\nested_sets\controllers;

use Yii;
use app\modules\nested_sets\models\Category;
use app\modules\nested_sets\models\search\CategoryItemSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CategoryItemController implements the CRUD actions for Category model.
 */
class CategoryItemController extends Controller
{
    /**
     * @var $rootMenu Category
     */
    public $rootMenu;
    public $defaultAction = 'index';
    public $layout = '@app/modules/admin/views/layouts/admin';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->rootMenu = Category::findOne(Yii::$app->request->get('root_id'));

    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoryItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->rootMenu);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'rootMenu' => $this->rootMenu,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'rootMenu' => $this->rootMenu,
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category([]);

        if ($model->load(Yii::$app->request->post())) {

            if($parent = Yii::$app->request->post('parent')) {
                $parent = Category::findOne(['id' => (int)$parent]);
                $model->prependTo($parent);
            } else {
                $model->prependTo($this->rootMenu);
            }

            if($insertAfter = Yii::$app->request->post('insert_after')) {
                $insertAfter = Category::findOne(['id' => (int)$insertAfter]);

                $model->insertAfter($insertAfter);
            }

            Yii::$app->session->setFlash('success', 'Запись успешно сохранена!');

            return $this->redirect(['update', 'id' => $model->id, 'root_id' => $this->rootMenu->id]);
        }

        $dropdownMenuAr = ArrayHelper::map($this->rootMenu->children()->all(), 'id', 'name');

        return $this->render('create', [
            'model' => $model,
            'dropdownMenuAr' => $dropdownMenuAr,
            'rootMenu' => $this->rootMenu,
            'modelParent' => false,
            'modelBefore' => false,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($parent = Yii::$app->request->post('parent')) {
                $parent = Category::findOne(['id' => (int)$parent]);
                $model->prependTo($parent);
            }

            if($insertAfter = Yii::$app->request->post('insert_after')) {
                $insertAfter = Category::findOne(['id' => (int)$insertAfter]);
                $model->insertAfter($insertAfter);
            }

            $model->save();

            Yii::$app->session->setFlash('success', 'Запись успешно сохранена!');

            return $this->redirect(['update', 'id' => $model->id, 'root_id' => $this->rootMenu->id]);
        }

        $dropdownMenuAr = ArrayHelper::map($this->rootMenu->children()->all(), 'id', 'name');
        unset($dropdownMenuAr[$model->id]);

        $modelParent = $model->parents(1)->one();

        $modelBefore = $model->prev()->one();

        return $this->render('update', [
            'model' => $model,
            'dropdownMenuAr' => $dropdownMenuAr,
            'modelParent' => $modelParent,
            'modelBefore' => $modelBefore,
            'rootMenu' => $this->rootMenu,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $model->deleteWithChildren();

        return $this->redirect(['index', 'root_id' => $this->rootMenu->id]);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
