<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\nested_sets\models\Category */
/* @var $rootMenu \app\modules\nested_sets\models\Category */

$this->title = 'Редактирование категории: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ['/admin/category-root/index']];
$this->params['breadcrumbs'][] = ['label' => $rootMenu->name, 'url' => ['index', 'root_id' => $rootMenu->id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id, 'root_id' => $rootMenu->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="nested-sets-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dropdownMenuAr' => $dropdownMenuAr,
        'modelParent' => $modelParent,
        'modelBefore' => $modelBefore,
        'rootMenu' => $rootMenu,
    ]) ?>

</div>
