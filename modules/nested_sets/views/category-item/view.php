<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\nested_sets\models\Category */
/* @var $rootMenu \app\modules\nested_sets\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['/admin/category-root/index']];
$this->params['breadcrumbs'][] = ['label' => $rootMenu->name, 'url' => ['index', 'root_id' => $rootMenu->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nested-sets-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'slug',
        ],
    ]) ?>

</div>
