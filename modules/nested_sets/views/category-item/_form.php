<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\modules\nested_sets\models\Category */
/* @var $form yii\widgets\ActiveForm */
/* @var $dropdownMenuAr array */
/* @var $modelParent \app\modules\nested_sets\models\Category|boolean */
/* @var $modelBefore \app\modules\nested_sets\models\Category|boolean */

?>

<div class="nested-sets-form">

    <?php if(Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success">
            <?=Yii::$app->session->getFlash('success')?>
        </div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col">
            <?= $form->field($model, 'permanent_slug')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="w-100"></div>

        <div class="col">
            <?= $form->field($model, 'parent', ['inputOptions' => ['name' => 'parent', 'class' => 'form-control']])->dropDownList($dropdownMenuAr,
                $modelParent ?
                    [
                        'prompt' => 'Выберите категорию',
                        'options' => [$modelParent->id => ['selected' => true]]
                    ]
                    :
                    [
                        'prompt' => 'Выберите категорию',
                    ]) ?>
        </div>

        <div class="col">
            <?= $form->field($model, 'insert_after', ['inputOptions' => ['name' => 'insert_after', 'class' => 'form-control']])->dropDownList($dropdownMenuAr,
                $modelBefore ?
                    [
                        'prompt' => 'Выберите категорию',
                        'options' => [$modelBefore->id => ['selected' => true]]
                    ]
                    :
                    [
                        'prompt' => 'Выберите категорию',
                    ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>