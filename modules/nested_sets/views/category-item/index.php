<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\nested_sets\models\Category;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\nested_sets\models\search\CategoryItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $rootMenu Category */

$this->title = 'Список категорий раздела "' . $rootMenu->name . '"';
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ['/admin/category-root/index']];
$this->params['breadcrumbs'][] = $rootMenu->name;
?>
<div class="nested-sets-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить категорию', ['create', 'root_id' => $rootMenu->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name',
                'value' => function(Category $model) {
                    $parents = ArrayHelper::getColumn($model->parents()->all(), function(Category $model) {
                        if (!$model->depth) {
                          return null;
                        }

                        return $model->name;
                    });
                    $parents[] = $model->name;

                    return implode(' => ', array_diff($parents, ['']));
                }
            ],
            //'tree',
            //'lft',
            //'rgt',
            //'depth',
            //'name',
            //'slug',
            //'created_at',
            //'updated_at',

            [
                'class' => 'app\components\MaterialActionColumn',
                'buttons' => [
                    'view' => function ($url, $model, $key) use($rootMenu) {
                        return Html::a('<span class="ti-eye"></span>', Url::toRoute(['/nested_sets/category-item/view', 'id' => $model->id, 'root_id' => $rootMenu->id]));
                    },
                    'update' => function ($url, $model, $key) use($rootMenu) {
                        return Html::a('<span class="ti-pencil"></span>', Url::toRoute(['/nested_sets/category-item/update', 'id' => $model->id, 'root_id' => $rootMenu->id]));
                    },
                    'delete' => function ($url, $model, $key) use($rootMenu) {
                        return Html::a('<span class="ti-trash"></span>',
                            Url::toRoute(['/nested_sets/category-item/delete', 'id' => $model->id, 'root_id' => $rootMenu->id]),
                            [
                                'data-method' => 'post',
                                'data-confirm' => 'Удаление записи так же удалит все вложенные записи!',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
</div>
