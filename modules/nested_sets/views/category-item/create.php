<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\nested_sets\models\Category */
/* @var $rootMenu \app\modules\nested_sets\models\Category */
/* @var $dropdownMenuAr array */

$this->title = 'Новая категория';
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ['/admin/category-root/index']];
$this->params['breadcrumbs'][] = ['label' => $rootMenu->name, 'url' => ['index', 'root_id' => $rootMenu->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nested-sets-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dropdownMenuAr' => $dropdownMenuAr,
        'modelParent' => false,
        'modelBefore' => false,
        'rootMenu' => $rootMenu,
    ]) ?>

</div>
