<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\nested_sets\models\NavigationMenu;

/* @var $this yii\web\View */
/* @var $model NavigationMenu */
/* @var $form yii\widgets\ActiveForm */
/* @var $dropdownMenuAr array */
/* @var $modelParent NavigationMenu|boolean */
/* @var $modelBefore NavigationMenu|boolean */
?>

<div class="navigation-menu-form">

    <?php if(Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success">
            <?=Yii::$app->session->getFlash('success')?>
        </div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-12">
            <?= $form->field($model, 'active')->radioList([0 => 'Не опубликовано', 1 => 'Опубликовано'],
                [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return Html::radio($name, $checked, ['id' => NavigationMenu::tableName().'-'.$index, 'value' => $value]) . '<label for="'.NavigationMenu::tableName().'-'.$index.'">' . $label . '</label>';
                    },
                ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col">
            <?= $form->field($model, 'parent', ['inputOptions' => ['name' => 'parent', 'class' => 'form-control']])->dropDownList($dropdownMenuAr,
                $modelParent ?
                    [
                        'prompt' => 'Выберите пункт меню',
                        'options' => [$modelParent->id => ['selected' => true]]
                    ]
                    :
                    [
                        'prompt' => 'Выберите пункт меню',
                    ]) ?>
        </div>

        <div class="col">
            <?= $form->field($model, 'insert_after', ['inputOptions' => ['name' => 'insert_after', 'class' => 'form-control']])->dropDownList($dropdownMenuAr,
                $modelBefore ?
                    [
                        'prompt' => 'Выберите пункт меню',
                        'options' => [$modelBefore->id => ['selected' => true]]
                    ]
                    :
                    [
                        'prompt' => 'Выберите пункт меню',
                    ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'category_id')->dropDownList($model->isNewRecord ? [] : ($model->category ? [$model->category_id => $model->category->name] : [])) ?>
        </div>

        <div class="col">
            <?= $form->field($model, 'page_id')->dropDownList($model->isNewRecord ? [] : ($model->page ? [$model->page_id => $model->page->title] : [])) ?>
        </div>

        <div class="col">
            <?= $form->field($model, 'permanent_slug')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-12">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>