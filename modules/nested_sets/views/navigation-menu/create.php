<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\nested_sets\models\NavigationMenu */
/* @var $dropdownMenuAr array */

$this->title = 'Новый пункт меню';
$this->params['breadcrumbs'][] = ['label' => 'Навигационное меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="navigation-menu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dropdownMenuAr' => $dropdownMenuAr,
        'modelParent' => false,
        'modelBefore' => false,
    ]) ?>

</div>
