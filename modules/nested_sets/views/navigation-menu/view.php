<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\nested_sets\models\NavigationMenu;

/* @var $this yii\web\View */
/* @var $model app\modules\nested_sets\models\NavigationMenu */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Навигационное меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="navigation-menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'active',
                'value' => function(NavigationMenu $model) {
                    return $model->active ? 'Опубликовано' : 'Не опубликовано';
                }
            ],
            [
                'attribute' => 'category_id',
                'value' => function(NavigationMenu $model) {
                    return $model->category ? $model->category->name : '(Не задано)';
                }
            ],
            [
                'attribute' => 'page_id',
                'value' => function(NavigationMenu $model) {
                    return $model->page ? $model->page->title : '(Не задано)';
                }
            ],
        ],
    ]) ?>

</div>
