<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\nested_sets\models\NavigationMenu */
/* @var $dropdownMenuAr array */
/* @var $modelParent \app\modules\nested_sets\models\NavigationMenu */
/* @var $modelBefore \app\modules\nested_sets\models\NavigationMenu */

$this->title = 'Редактирование: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Навигационное меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="navigation-menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dropdownMenuAr' => $dropdownMenuAr,
        'modelParent' => $modelParent,
        'modelBefore' => $modelBefore,
    ]) ?>

</div>
