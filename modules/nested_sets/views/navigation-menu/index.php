<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\widgets\Script;
use app\modules\nested_sets\models\NavigationMenu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\nested_sets\models\search\NavigationMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Навигационное меню';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="navigation-menu-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новый пункт меню', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'name',
                'value' => function( NavigationMenu $model) {
                    $parents = \yii\helpers\ArrayHelper::getColumn($model->parents()->all(), function(NavigationMenu $model) {
                        if (!$model->depth) {
                            return null;
                        }

                        return $model->name;
                    });
                    $parents[] = $model->name;

                    return implode(' => ', array_diff($parents, ['']));
                }
            ],
            [
                'attribute' => 'active',
                'filter' => [0 => 'Не опубликовано', 1 => 'Опубликовано'],
                'filterInputOptions' => ['prompt' => 'Выберите статус', 'class' => 'form-control'],
                'value' => function(NavigationMenu $model) {
                    return $model->active ? 'Опубликовано' : 'Не опубликовано';
                }
            ],
            [
                'attribute' => 'category.name',
                'label' => 'Название категории',
                'filter' => [(Yii::$app->request->get($searchModel->formName())['category.name'] ?? '') => (Yii::$app->request->get($searchModel->formName())['category.name'] ?? '')],
                'filterInputOptions' => ['id' => 'filter-category-name', 'class' => 'form-control',],
            ],
            [
                'attribute' => 'page.title',
                'label' => 'Название страницы',
                'filter' => [(Yii::$app->request->get($searchModel->formName())['page.title'] ?? '') => (Yii::$app->request->get($searchModel->formName())['page.title'] ?? '')],
                'filterInputOptions' => ['id' => 'filter-page-title', 'class' => 'form-control'],
            ],

            ['class' => 'app\components\MaterialActionColumn'],
        ],
    ]); ?>
</div>

<?php Script::begin(); ?>
<script>

</script>
<?php Script::end(); ?>