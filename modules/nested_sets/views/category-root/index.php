<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\nested_sets\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\nested_sets\models\search\CategoryRootSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Разделы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nested-sets-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новый раздел', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name',
                'value' => function(Category $model) {
                    $parents = ArrayHelper::getColumn($model->parents()->all(), function(Category $model) {
                        if(!$model->depth)
                            return null;

                        return $model->name;
                    });
                    $parents[] = $model->name;

                    return implode(' => ', array_diff($parents, ['']));
                }
            ],

            [
                'class' => 'app\components\MaterialActionColumn',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="ti-eye"></span>', \yii\helpers\Url::toRoute(['/nested_sets/category-item/index', 'root_id' => $model->id]));
                    }
                ],
            ],
        ],
    ]); ?>
</div>
