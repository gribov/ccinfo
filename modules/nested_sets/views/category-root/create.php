<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\nested_sets\models\Category */

$this->title = 'Новый раздел';
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nested-sets-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
