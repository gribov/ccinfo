<?php

namespace app\modules\nested_sets\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;
use yii\db\ActiveQuery;
/**
 * Class NestedSetsQuery
 * @package app\modules\nested_sets\models
 * @see NestedSets
 */
class NestedSetsQuery extends ActiveQuery
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
}