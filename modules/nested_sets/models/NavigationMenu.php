<?php

namespace app\modules\nested_sets\models;

use app\modules\admin\models\Page;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "navigation_menu".
 *
 * @property int $id
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property string $name
 * @property string $slug
 * @property string $permanent_slug
 * @property int $active
 * @property int $category_id
 * @property int $page_id
 * @property integer $parent
 * @property integer $insert_after
 * @property Category $category
 * @property Page $page
 * @property Page $pageWithoutContent
 *
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class NavigationMenu extends \yii\db\ActiveRecord
{
    const ROOT_ID = 'glavnoe-menu';
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * ID родительской категории
     * @var $parent int
     */
    public $parent;
    /**
     * ID категории, после которой будет вставлена текущая
     * @var $insert_after int
     */
    public $insert_after;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
          TimestampBehavior::class,
          BlameableBehavior::class,
            'tree' => [
                'class' => NestedSetsBehavior::class,
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'depth',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
            'slug' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new NestedSetsQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'navigation_menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['active', 'category_id', 'page_id'], 'default', 'value' => null],
            [['active', 'category_id', 'page_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['permanent_slug'], 'string', 'max' => 500],
            [['permanent_slug'], 'url'],
            [['category_id', 'page_id'], 'atLeastOne', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'Depth',
            'name' => 'Название',
            'slug' => 'Slug',
            'active' => 'Статус',
            'category_id' => 'Прикрепленная категория',
            'page_id' => 'Прикрепленная страница',
            'parent' => 'Родительский пункт меню',
            'insert_after' => 'Вставить после пункта меню',
            'permanent_slug' => 'Алиас, по которому будет доступна страница',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeHints()
    {
        return [
            'permanent_slug' => 'Можно вставить ссылку на сторонний ресурс.',
        ];
    }

    /**
     * Связанная модель привязанной категории
     * @return ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * Связанная модель привязанной страницы
     * @return ActiveQuery
     */
    public function getPage() {
        return $this
            ->hasOne(Page::class, ['id' => 'page_id'])
            ->select(['id', 'title', 'slug']);
    }

    /**
     * Связанная модель привязанной активной страницы
     * @return ActiveQuery
     */
    public function getActivePage() {
        return $this
            ->hasOne(Page::class, ['id' => 'page_id'])
            ->select(['id', 'title', 'slug'])
            ->where([Page::tableName().'.active' => Page::STATUS_ACTIVE]);
    }

    /**
     * Отображает вложенное меню
     * @param $tree
     * @param string $css_class
     * @param int $currDepth
     * @return string
     */
    public static function renderNestedSets($tree, $css_class = 'menu', $currDepth = 1)
    {
        $result = '<ul class="' . $css_class . '">';

        $depthsLinks = [];

        foreach ($tree as $item) {

            $path = (($item->depth > $currDepth) ? $depthsLinks[$item->depth - 1] : '') . $item->slug;
            $depthsLinks[$item->depth] = $path . '/';

            if ($item->depth > $currDepth) {
                $result .= '<li><ul>'; // open sub tree if level up
            }

            if ($item->depth < $currDepth) {
                $result .= str_repeat("</ul></li>", $currDepth - $item->depth); // close sub tree if level down
            }

            $result .= '<li><a href="' . $depthsLinks[$item->depth] .'">' . $item->name . '</a></li>';
            $currDepth = $item->depth;
        }
        $result .= "</ul>";

        return $result;
    }

    /**
     * Вложенный массив с иерархией
     * @param $tree
     * @return array
     */
    public static function arrayNestedSets(array $tree) {
        $stack = [];
        $arraySet = [];
        foreach ($tree as $intKey => $arrValues) {

            if(isset($arrValues['permanent_slug']) && $arrValues['permanent_slug']) {
                $arrValues['slug'] = $arrValues['permanent_slug'];
                $arrValues['permanent_slug'] = true;
            }

            $stackSize = count($stack);

            while ($stackSize > 0 && $stack[$stackSize - 1]['rgt'] < $arrValues['lft']) {
                array_pop($stack);
                $stackSize--;
            }

            $link =& $arraySet;
            for($i = 0; $i < $stackSize; $i++) {
                $link =& $link[$stack[$i]['id']]['children'];
            }

            $tmp = array_push($link, ['item' => $arrValues, 'children' => []]);
            array_push($stack, ['id' => $tmp - 1, 'rgt' => $arrValues['rgt']]);

        }
        return $arraySet;
    }

    public function atLeastOne($attribute, $params, $validator)
    {
        if(!$this->category_id && !$this->page_id) {
            $this->addError($attribute, 'Поле обязательно к заполнению.');
        }

    }
}
