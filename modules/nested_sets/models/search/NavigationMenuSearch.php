<?php

namespace app\modules\nested_sets\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\nested_sets\models\NavigationMenu;
use yii\db\ActiveQuery;

/**
 * NavigationMenu represents the model behind the search form of `app\modules\nested_sets\models\NavigationMenu`.
 */
class NavigationMenuSearch extends NavigationMenu
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['category.name', 'page.title']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'lft', 'rgt', 'depth', 'active', 'category_id', 'page_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'slug', 'category.name', 'page.title'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NavigationMenu::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['category']);
        $query->joinWith(['page']);

        $dataProvider->sort->defaultOrder['created_at'] = ['asc' => ['created_at' => SORT_ASC], 'desc' => ['created_at' => SORT_DESC]];
        $dataProvider->sort->attributes['category.name'] = ['asc' => ['category.name' => SORT_ASC], 'desc' => ['category.name' => SORT_DESC]];
        $dataProvider->sort->attributes['page.title'] = ['asc' => ['page.title' => SORT_ASC], 'desc' => ['page.title' => SORT_DESC]];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andWhere(['!=', 'navigation_menu.depth', 0]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lft' => $this->lft,
            'rgt' => $this->rgt,
            'navigation_menu.depth' => $this->depth,
            'navigation_menu.active' => $this->active,
            'category_id' => $this->category_id,
            'page_id' => $this->page_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'slug', $this->slug]);

        $query->andFilterWhere(['ilike', 'category.name', $this->getAttribute('category.name')]);
        $query->andFilterWhere(['ilike', 'page.title', $this->getAttribute('page.title')]);

        return $dataProvider;
    }
}
