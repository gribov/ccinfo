<?php
use yii\helpers\Url;

$this->title = 'Вход';
?>

<div class="modal fade" id="summary" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Тестовый вход</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php foreach ($users as $user) : ?>
                    <a class="btn btn-block btn-white btn-custom waves-effect" href="<?= Url::to(['/admin/admin/test-login', 'user_id' => $user->id]); ?>"><?= $user->username ?></a><br><br>
                <?php endforeach; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
