<?php
namespace app\widgets;

use yii\base\Widget;
use app\models\User;

class TestLoginWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('test-login', [
            'users' => User::find()->where(['in', 'username', ['test_user', 'test_admin', 'test_operator']])->all()
        ]);
    }
}