<?php

namespace app\commands;

use Yii;

class JenkinsController extends \yii\console\controllers\MigrateController
{
	public function actionUp($limit = 0)
    {
    	//yii jenkins --interactive=0
    	$EXIT_CODE = parent::actionUp();
    	$this->stdout("\n" . !$EXIT_CODE);
        return $EXIT_CODE;
    }

    public function actionVersion()
    {
        $this->stdout(Yii::$app->params['app_version']);
    }
}
