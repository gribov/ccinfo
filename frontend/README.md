# ccinfo

> CcInfo vueJs project

## Build Setup

``` bash
# install dependencies
npm install

# serve with API stub for local dev
npm run serve
npm run dev-local

# serve with hot reload at localhost:8080 and test-ccinfo php API
npm run dev

# build for production with minification
npm run build
```

Система взаимодействует с API news.intranet, параметры хоста которой заданы в package.json в разделе scripts.
API с которым взаимодействует система при режиме dev - test.ccinfo.intranet - это тоже есть в package.json.

#### Как построена разработка
 - из /frontend запускаем webpack-dev-server `npm run dev`
 - Поднимается сервер на localhost:8080
 - Каждое изменение файлов из директории src будет отражено в браузере (Hot Module Replacement)
 - После того, как работа сделана и на localhost:8080 сайт выглядит как требуется, необходимо пересобрать проект - `npm run build`
 - Будет запущен сборка и файлы нужно закоммитить
 
 
