const fs = require('fs');
const path = require('path');
let accessToken = 'token';
const REFRESH_TOKEN = 'refresh';

const authMiddleware = function(req, res, next) {
  if (!req.headers.authorization || req.headers.authorization !== 'Bearer ' + accessToken) {
    res.statusCode = 401;
    return res.json('Not Authorize');
  }
  next();
};

module.exports = function (app) {

  app.post('/api/v1/404', function (req, res, next) {
    res.statusCode = 404;
    return res.json({message: 'Not Found'});
  });

  app.get('/api/v1/menu', function (req, res, next) {
    return fs.readFile(path.resolve(__dirname, 'data', 'menu.json'), function (err, data) {
      return res.json(JSON.parse(data));
    });
  });

  app.get('/api/v1/page/slug/:slug', function (req, res, next) {
    const slug = req.params.slug;
    return fs.readFile(path.resolve(__dirname, 'data', 'page_' + slug + '.json'), function (err, data) {
      if (err) {

        return fs.readFile(path.resolve(__dirname, 'data', 'page_test.json'), function (err, data) {
          res.json(JSON.parse(data));

        });

      }
      return res.json(JSON.parse(data));
    });
  });

  app.get('/api/v1/category/slug/:slug', function (req, res, next) {
    const slug = req.params.slug;
    return fs.readFile(path.resolve(__dirname, 'data', 'category_' + slug + '.json'), function (err, data) {
      if (err) {
        return fs.readFile(path.resolve(__dirname, 'data', 'category_test.json'), function (err, data) {
          res.json(JSON.parse(data));

        });
      }
      return res.json(JSON.parse(data));
    });
  });

  app.get('/api/v1/widgets/slider/:slug', function (req, res, next) {
    const slug = req.params.slug;
    return fs.readFile(path.resolve(__dirname, 'data', 'slider_' + slug + '.json'), function (err, data) {
      if (err) {
        res.statusCode = 404;
        return res.json({message: 'Not Found'});
      }
      return res.json(JSON.parse(data));
    });
  });

  app.get('/api/v1/widgets/competition', function (req, res, next) {
    const slug = req.params.slug;
    return fs.readFile(path.resolve(__dirname, 'data', 'competition.json'), function (err, data) {
      if (err) {
        res.statusCode = 404;
        return res.json({message: 'Not Found'});
      }
      return res.json(JSON.parse(data));
    });
  });

  app.get('/api/v1/auth', function (req, res, next) {

    if (req.query.redirect_url) {
      return res.redirect(req.query.redirect_url + '?code=code')
    }
    res.statusCode = 400;

    return res.json({message: 'Bad Request'});
  });

  app.get('/api/v1/token', function (req, res, next) {
    if (!req.query.code) {
      res.statusCode = 400;
      return res.json({message: 'Bad Request'});
    }
    return res.json({access_token: accessToken, refresh_token: REFRESH_TOKEN});
  });

  app.get('/api/v1/token/refresh', function (req, res, next) {
    if (
      !req.query.refresh_token
      || req.query.refresh_token !== REFRESH_TOKEN
    ) {
      res.statusCode = 400;

      return res.json({message: 'Bad Request'});
    }

    return res.json({access_token: accessToken, refresh_token: REFRESH_TOKEN});
  });

  app.get('/api/v1/profile', authMiddleware, function (req, res, next) {
    return res.json({username: 'test', name: 'John Tests', email: 'mail@mail.com', roles: ['admin']});
  });

  const links = [
    {id: 1, route: "/category/otdeleniabankomaty", title: "test - 1"},
    {id: 2, route: "/page/obsluzivanie", title: "test - 2"},
    {id: 3, route: "/", title: "home"},
  ];

  app.get('/api/v1/user-saved-link', authMiddleware, function (req, res, next) {
    return res.json(
      links.filter(function (link) {
        return link !== null;
      })
    );

  });

  app.get('/api/v1/categories', function (req, res, next) {
    return res.json([
      {id:1, name: 'Физические лица', birthDate: '12 марта'},
      {id:2, name: 'Юридические лица', birthDate: '12 марта'},
      {id:3, name: 'Информация', birthDate: '12 марта'},
    ]);
  });

  app.get('/api/v1/widgets/birthday-list', function (req, res, next) {

    return res.json([
      {name: 'Иванов Иван', birthDate: '12 марта'},
      {name: 'Петров Петр', birthDate: '12 марта'},
      {name: 'Татьянина Татьяна', birthDate: '12 марта'},
    ]);
  });

  app.post('/api/v1/user-saved-link', function (req, res, next) {
    const link = {id: 4, route: req.body.route, title: req.body.title};
    links.push(link);
    res.statusCode = 200;
    return res.json(link);
  });


  app.delete('/api/v1/user-saved-link/:id', authMiddleware, function (req, res, next) {
    // shit!))
    links.forEach(function (link, indx) {
      if (link.id == req.params.id) {
        delete links[indx];
      }
    });

    res.statusCode = 200;
    return res.json();
  });

  let respUser = {
    from: "2018-03-29T01:00:00+0300",
    to: "2018-03-29T10:00:00+0300",
    phone: "1111",
    user: "Ilya A. Gribov"
  };
  app.get('/api/v1/responsible-user', function (req, res, next) {
    return res.json(respUser);
  });

  app.post('/api/v1/responsible-user', authMiddleware, function (req, res, next) {
    respUser = {user: "Ilya A. Gribov", ...req.body};
    return res.json(respUser);
  });

  app.get('/api/v1/categories', function (req, res, next) {

    return fs.readFile(path.resolve(__dirname, 'data', 'news_categories.json'), function (err, data) {
      if (err) {
        res.statusCode = 404;
        return res.json({message: 'Not Found'});
      }
      return res.json(JSON.parse(data));
    });
  });

  app.get('/api/v1/categories/:id', function (req, res, next) {
    return fs.readFile(path.resolve(__dirname, 'data', 'news_category.json'), function (err, data) {
      if (err) {
        res.statusCode = 404;
        return res.json({message: 'Not Found'});
      }
      return res.json(JSON.parse(data));
    });
  });


  app.get('/api/v1/news', function (req, res, next) {
    const page = (+req.query.page || 1) % 2;

    if (+req.query.page > 10) {
      return res.json([]);
    }

    return fs.readFile(path.resolve(__dirname, 'data', 'news_items' + page + '.json'), function (err, data) {
      if (err) {
        res.statusCode = 404;
        return res.json({message: 'Not Found'});
      }
      return res.json(JSON.parse(data));
    });
  });


  app.get('/api/v1/widgets/birthday-list', function (req, res, next) {
    return res.json([
      {"name": "Гладкова Дарина", "birthDate": "22 ноября"},
      {"name": "Николай Андреев", "birthDate": "23 ноября"},
      {"name": "Илья Грибов", "birthDate": "19 апреля"}
      ]);
  });

};