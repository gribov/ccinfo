import {mapMutations} from 'vuex';

/**
 * Mixin for http request with vue resource
 * Requests Category or Page by slug
 */
export default {
  data() {
    return {
      pageNotFound: null,
      pageError: null,
    }
  },
  methods: {
    requestResource(q) {
      this.$Progress.start();

      return this.resource.get(q)
        .then(response => {
          return response.json();
        })
        .catch(res => {
          if (res.status >= 500 || res.status === 0) {
            this.pageError = true;
            return;
          }
          if (!res.ok && res.status === 404) {
            this.pageNotFound = true;
          }
        })
    },
  }
};
