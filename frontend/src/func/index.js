export const FORMAT_DMY = 'd.m.y';

export function getDateString(value, format = 'full') {
  const date = typeof value === 'Object' ? value :  new Date(value.split(' ').join('T'));

  const month = date.getMonth() < 10 ? '0' + (date.getMonth()+1) : (date.getMonth() + 1),
    dateVal = date.getDate() < 9 ? '0' + date.getDate() + 1 : date.getDate() + 1,
    hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours(),
    minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes(),
    seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();

  switch (format) {
    case 'd.m.y':
      return `${dateVal}.${month}.${date.getFullYear()}`;
    case FORMAT_DMY:
    default:
      return `${date.getFullYear()}-${month}-${dateVal} ${hours}:${minutes}:${seconds}`;
  }

}