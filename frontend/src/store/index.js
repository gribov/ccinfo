import Vue from 'vue'
import Vuex from 'vuex'
import elements from './modules/elements';
import user from './modules/user';
import modal from './modules/modal';
import responsibleUser from './modules/responsible_user';
import settings from './modules/settings';

Vue.use(Vuex);
const strict = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    elements,
    user,
    modal,
    settings,
    'responsible_user': responsibleUser
  },
  strict,
})