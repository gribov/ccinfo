import Vue from 'vue';
import {RESPONSIBLE_USER_ENDPOINT} from '../../resource';

const state = {
  responsibleUser: null
};

const getters = {
  responsibleUser: state => state.responsibleUser
};

const mutations = {
  setResponsibleUser: (state, payload) => state.responsibleUser = payload
};

const actions = {
  changeResponsibleUser({commit, dispatch}, payload) {
    Vue.resource(RESPONSIBLE_USER_ENDPOINT).save({}, payload)
      .then(response => response.json())
      .then(data => commit('setResponsibleUser', data));
  },
  getResponsibleUser({commit, dispatch}) {
    Vue.resource(RESPONSIBLE_USER_ENDPOINT)
      .get()
      .then(res => res.json())
      .then(user => commit('setResponsibleUser', user));
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};