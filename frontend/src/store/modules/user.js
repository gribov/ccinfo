import * as config from '../../config';
import Vue from 'vue';
import * as apiEndpoints from '../../resource';
import _ from 'lodash';

const state = {
  userLogin: false,
  user: {
    username: null,
    name: null,
    email: null,
    roles: [],
  },
  userSavedLinks: []
};

const getters = {
  userLogin: state => state.userLogin,
  user: state => state.user,
  userSavedLinks: state => state.userSavedLinks,
  isAdmin: state => state.user.roles.indexOf('admin') !== -1,
  userCanBeResponsible: state => state.user.roles.indexOf('responsible') !== -1,
};

const mutations = {
  setToken(state, payload) {
    localStorage.setItem('access_token', payload.access_token);
    localStorage.setItem('refresh_token', payload.refresh_token);
  },

  setUserData(state, payload) {
    state.user.username = payload.username;
    state.user.name = payload.name;
    state.user.email = payload.email;
    state.user.roles = payload.roles || [];
    state.userLogin = true;
  },

  userLogout(state) {
    state.user.username = null;
    state.user.name = null;
    state.user.email = null;
    state.user.roles = [];
    state.userLogin = false;

    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
  },

  setUserSavedLinks(state, payload) {
    state.userSavedLinks = payload;
  },

  deleteUserSavedLink(state, payload) {
    state.userSavedLinks.splice(state.userSavedLinks.indexOf(payload), 1);
  },
};

const actions = {
  // request access_token by code, then request profile by token
  authorizeByApi({commit, dispatch}, payload) {
    Vue.resource(apiEndpoints.TOKEN_ENDPOINT)
      .get({
        code: payload.code,
        grant_type: 'authorization_code',
        app_name: config.API_APP_NAME,
        app_key: config.API_APP_KEY,
        redirect_url: payload.redirect_url
      })
      .then(({data}) => {
        commit('setToken', data);
        window.history.pushState({}, document.title, window.location.pathname);
        dispatch('requestProfile');
        dispatch('fetchUserSavedLinks');
      })
      .catch(err => console.error(err))
  },

  requestProfile({commit}) {
    Vue.resource(apiEndpoints.PROFILE_ENDPOINT)
      .get()
      .then(({data}) => {
        commit('setUserData', data);
      })
      .catch(err => console.error(err));
  },

  fetchUserSavedLinks({commit}) {
    Vue.resource(apiEndpoints.SAVED_LINKS_ENDPOINT)
      .get()
      .then(response => response.json())
      .then(links => _.orderBy(links, 'id', 'desc'))
      .then(links => commit('setUserSavedLinks', links))
      .catch(err => console.error(err));
  },

  addUserSavedLink({commit, dispatch}, payload) {
    Vue.resource(apiEndpoints.SAVED_LINKS_ENDPOINT).save({}, payload)
      .then(response => response.json())
      .then(() => dispatch('fetchUserSavedLinks'));
  },

  removeUserSavedLink({commit, dispatch}, payload) {
    commit('deleteUserSavedLink', payload);
    Vue.resource(apiEndpoints.SAVED_LINKS_ENDPOINT)
      .delete({id: payload.id})
      .then(() => dispatch('fetchUserSavedLinks'));
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};