const state = {
  navbarOpen: false,
  paginationPage: 1,
  paginationNextPageDisabled: false,
};

const getters = {
  navbarOpen: state => state.navbarOpen,
  paginationPage: state => state.paginationPage,
  paginationNextPageDisabled: state => state.paginationNextPageDisabled,
};

const mutations = {
  openNavBar: state => state.navbarOpen = true,
  closeNavBar: state => state.navbarOpen = false,
  toggleNavBar: state => state.navbarOpen = !state.navbarOpen,

  paginationSetPage: (state, payload) => state.paginationPage = payload,
  paginationSetNexPageDisabled: (state, payload) => state.paginationNextPageDisabled = payload,
};

export default {
  namespaced: true,
  state,
  getters,
  mutations
};