import Vue from 'vue';
import {SETTINGS_ENDPOINT} from '../../resource';

const state = {
  background: false
};

const getters = {
  background: state => {
    if (!state.background) {
      return '';
    }
    if (state.background.mode === 'image') {

      if (state.background.repeat === '0') {
        return {
          'background-image': `url("${state.background.url}")`,
          'background-repeat': 'no-repeat',
          'background-size': 'contain',
          'background-position': 'center'
        };
      } else {
        return {
          'background': `url("${state.background.url}") repeat`
        };
      }
    }

    return {
      'background-color': state.background.color
    };
  }
};

const mutations = {
  setBackground: (state, payload) => state.background = payload,
};

const actions = {
  requestSettings({commit}) {
    Vue.resource(SETTINGS_ENDPOINT)
      .get()
      .then(res => res.json())
      .then(data => {
        commit('setBackground', data.background);
      })
      .catch(err => console.error(err));
  }
};



export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};