const state = {
  smallModal: {
    open: false,
    content: {
      title: '',
      body: '',
    },
    confirm: null
  },
  modal: {
    open: false,
    title: '',
    text: '',
    component: null,
    componentProps: {}
  },
};

const getters = {
  smallModalOpen: state => state.smallModal.open,
  smallModalContent: state => state.smallModal.content,

  modalTitle: state => state.modal.title,
  modalComponent: state => state.modal.component,
  modalComponentProps: state => state.modal.componentProps,
  modalOpen: state => state.modal.open,
};

const mutations = {
  smallModalOpen(state, {title = '', body = '', confirm = null}) {
    state.smallModal.content.title = title;
    state.smallModal.content.body = body;
    state.smallModal.confirm = confirm;
    state.smallModal.open = true;
  },
  smallModalClose: state => state.smallModal.open = false,
  smallModalConfirm: state => {
    if(state.smallModal.confirm){
      state.smallModal.confirm();
    }
    state.smallModal.open = false;
  },
  /* modal window */
  openModal(state, {title = '', componentProps = {}, component}) {
    state.modal.title = title;
    state.modal.componentProps = componentProps;
    state.modal.component = component;
    state.modal.open = true;
  },
  closeModal: state => state.modal.open = false,
};


export default {
  namespaced: true,
  state,
  getters,
  mutations,
};