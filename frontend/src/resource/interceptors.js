import {PAGE_ENDPOINT, CATEGORY_ENDPOINT} from './index';
import app from '../main';

const changeTitleResources = [
  PAGE_ENDPOINT,
  CATEGORY_ENDPOINT
];

export const changePageTitleInterceptor = (request, next) => {
  if (changeTitleResources.indexOf(request.url) === -1) {
    next();
  } else {
    next(response => {
      if (!response.ok) {
        return;
      }
      let documentTitle = 'Информационный портал';
      switch (request.url) {
        case PAGE_ENDPOINT:
          documentTitle = response.data.title;
          break;
        case CATEGORY_ENDPOINT:
          documentTitle = response.data.item.name;
          break;
        default:
          break;
      }
      document.title = documentTitle;
    })
  }
};

export const requestResourceWithProgressBarInterceptor = (request, next) => {
  if (changeTitleResources.indexOf(request.url) === -1) {
    next();
  } else {
    if (app) {
      app.$Progress.start();
    }
    next(response => {
      if (app) {
        app.$Progress.finish();
      }
    })
  }
};