export const NAV_MENU_ENDPOINT = 'menu';
export const PAGE_ENDPOINT = 'page/slug/{slug}';
export const CATEGORY_ENDPOINT = 'category/slug/{slug}';
export const CAROUSEL_ENDPOINT = 'widgets/slider/{slug}';
export const COMPETITION_ENDPOINT = 'widgets/competition';
export const BIRTHDAY_ENDPOINT = 'widgets/birthday-list';
export const TOKEN_ENDPOINT = 'token';
export const TOKEN_REFRESH_ENDPOINT = 'token/refresh';
export const PROFILE_ENDPOINT = 'profile';
export const SAVED_LINKS_ENDPOINT = 'user-saved-link{/id}';
export const RESPONSIBLE_USER_ENDPOINT = 'responsible-user';
export const SETTINGS_ENDPOINT = 'widgets/settings';
export const SEARCH_ENDPOINT = 'search';

/* global NEWS_API_HOST */
export const NEWS_CATEGORIES_ENDPOINT = `${NEWS_API_HOST}/api/v1/categories{/id}`;
export const NEWS_NEWS_ENDPOINT = `${NEWS_API_HOST}/api/v1/news{/id}`;
