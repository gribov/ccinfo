import Vue from 'vue';
import App from './App.vue';
import VueProgressBar from 'vue-progressbar';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';

import store from './store';
import router from './router';
import * as config from './config';
import {refreshAccessTokenInterceptor} from './auth';
import {changePageTitleInterceptor, requestResourceWithProgressBarInterceptor} from './resource/interceptors';

import * as filters from './filters';

Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(VueProgressBar, {
  color: '#fff',
  failedColor: 'grey',
  height: '4px',
  transition: {speed: '0.5s', opacity: '1s', termination: 800}
});

Vue.http.options.root = config.API_ROOT;
Vue.http.interceptors.push(refreshAccessTokenInterceptor, changePageTitleInterceptor, requestResourceWithProgressBarInterceptor);

Vue.filter('capitalize', filters.capitalize);
Vue.filter('cut', filters.cut);
Vue.filter('dateFilterDMY', filters.dateFilterDMY);
Vue.filter('slash', filters.slash);


export default new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
});
