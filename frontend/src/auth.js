import Vue from 'vue';
import * as resourceRoutes from './resource';
/* global API_HOST */

const STATUS_NOT_AUTHORIZE = 401;
// interceptor.
export const refreshAccessTokenInterceptor = (request, next) => {
  const token = localStorage.getItem('access_token');
  if (token) {
    request.headers.set('Authorization', `Bearer ${token}`);
  }
  next(response => {
    const responseUrl = response.url.replace(API_HOST, '');
    if (responseUrl !== '/api/v1/token/refresh' && (response.status === STATUS_NOT_AUTHORIZE)) {
      return refreshAccessToken().then(() => Vue.http(request)).catch(() => response);
    }
    return response;
  });
};

export const refreshAccessToken = () => {
  if (!localStorage.getItem('refresh_token')) {
    return new Promise((res, rej) => rej('ERROR'));
  }
  return Vue.resource(resourceRoutes.TOKEN_REFRESH_ENDPOINT).get({
    refresh_token: localStorage.getItem('refresh_token'),
    grant_type: 'refresh_token',
  }).then(({data}) => {
    localStorage.setItem('access_token', data.access_token);
    localStorage.setItem('refresh_token', data.refresh_token);
  })
    .catch(() => {
      // logout user
      localStorage.removeItem('access_token');
      localStorage.removeItem('refresh_token');
    });
};