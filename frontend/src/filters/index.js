import {getDateString, FORMAT_DMY} from '../func';
import moment from 'moment-mini';

export const capitalize = value => value ? value.toString().charAt(0).toUpperCase() + value.toString().slice(1) : '';
export const cut = (value, length) => value.length < length ? value : `${value.slice(0, length)}. . .`;
export const dateFilterDMY = value => moment(value).format('YYYY.MM.DD');
export const slash = val => val ? ' / ' + val : '';
