import VueRouter from 'vue-router';
import app from '../main';
import routes from './routes';

const routesWithRequest = ['page', 'category'];

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
});

router.beforeEach((to, from, next) => {
  router.app.$store.commit('elements/closeNavBar');
  next();
});

router.beforeEach((to, from, next) => {
  document.title = 'Информационный портал';
  next();
});

router.beforeEach((to, from, next) => {
  if (app && routesWithRequest.indexOf(to.name) === -1) {
    app.$Progress.start();
  }
  next();
});

router.afterEach((to) => {
  if (app && routesWithRequest.indexOf(to.name) === -1) {
    app.$Progress.finish();
  }
});

export default router;