import store from '../store';
import router from '../router';

import Home from '../components/pages/Home.vue';

const Category = resolve => {
  require.ensure(['../components/pages/Category.vue'], () => {
    resolve(require('../components/pages/Category.vue'));
  }, 'content');
};

const Page = resolve => {
  require.ensure(['../components/pages/Page.vue'], () => {
    resolve(require('../components/pages/Page.vue'));
  }, 'content');
};

const NewsPage = resolve => {
  require.ensure(['../components/pages/NewsPage.vue'], () => {
    resolve(require('../components/pages/NewsPage.vue'));
  }, 'content');
};

const NewsCategory = resolve => {
  require.ensure(['../components/pages/NewsCategory.vue'], () => {
    resolve(require('../components/pages/NewsCategory.vue'));
  }, 'content');
};

const NotFound = () => import(/* webpackChunkName: "notfound" */'../components/pages/NotFound.vue');
const Search = () => import(/* webpackChunkName: "search" */'../components/pages/Search.vue');

const Account = resolve => {
  require.ensure(['../components/account/Account.vue'], () => {
    resolve(require('../components/account/Account.vue'));
  }, 'user');
};

const Profile = resolve => {
  require.ensure(['../components/account/Profile.vue'], () => {
    resolve(require('../components/account/Profile.vue'));
  }, 'user');
};

const Login = resolve => {
  require.ensure(['../components/account/Login.vue'], () => {
    resolve(require('../components/account/Login.vue'));
  }, 'user');
};

const Logout = resolve => {
  require.ensure(['../components/account/Logout.vue'], () => {
    resolve(require('../components/account/Logout.vue'));
  }, 'user');
};

export default [
  {
    path: '',
    component: Home,
    name: 'home'
  },
  {
    name: 'category',
    path: '/category/:slug',
    component: Category,
  },
  {
    name: 'page',
    path: '/page/:slug',
    component: Page,
  },
  {
    name: 'news',
    path: '/news/:id',
    component: NewsPage,
  },
  {
    name: 'news-category',
    path: '/news-category/:category_id',
    alias: '/news-root-category/:category_id',
    component: NewsCategory,
  },
  {
    path: '/news-root-category/',
    component: NewsCategory,
  },
  {
    path: '/search',
    component: Search,
    name: 'search'
  },
  {
    path: '/account',
    component: Account,
    beforeEnter(to, from, next) {
      if (!store.getters['user/userLogin'] && to.name !== 'account-login') {
        router.push({name: 'account-login'});
      }
      next();
    },
    children: [
      {path: '',  name: 'account-profile', component: Profile},
      { path: 'login', name: 'account-login', component: Login},
      { path: 'logout', name: 'account-logout', component: Logout},
    ]
  },
  {
    path: '/404',
    component: NotFound,
    name: '404'
  },
  {
    path: '*',
    component: NotFound,
  },
];