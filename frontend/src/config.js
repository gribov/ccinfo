/*global API_HOST*/
export const API_ROOT = `${API_HOST}/api/v1`;
export const API_APP_NAME = 'my_local_app';
export const API_APP_KEY = 'my_local_app_key';