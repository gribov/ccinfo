const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const router = require('./backend_stub/router');
const cors = require('cors');

//init App
const app = express();
//App setup
app.use(cors({origin: 'http://localhost:8080'}));
app.use(bodyParser.json({type: '*/*'}));

const publicPath = path.resolve(__dirname, 'backend_stub', 'public');
app.use('/', express.static(publicPath));
router(app);

//Server setup
const port = process.env.PORT || 3000;
const server = http.createServer(app);
server.listen(port);
console.log('SERVER LISTENING ON PORT ', port);
