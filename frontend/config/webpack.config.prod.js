const webpack = require('webpack');
const commonConfig = require('./webpack.config.common.js');
const merge = require('webpack-merge');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const outputDir = path.resolve(__dirname, '../../web/frontend');

const prodConfig = {
  devtool: '#source-map',
  output: {
    path: outputDir,
  },
  plugins: [
    new CleanWebpackPlugin(['*'], {
      root: outputDir
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ]
};

module.exports = merge(commonConfig, prodConfig);
