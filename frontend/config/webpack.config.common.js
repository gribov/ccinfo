const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const frontendWebDir = path.resolve(__dirname, '../web/frontend');

module.exports = {
  entry: {
    main: './src/main.js'
  },
  output: {
    path: path.resolve(__dirname, './../dist'),
    publicPath: '/',
    filename: 'public/[name].js',
    chunkFilename: 'public/[name].bundle.[hash].js',
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.vue$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        include: [path.resolve(__dirname, './../src/assets')],
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      },
      {
        test: /\.css$/,
        include: [path.resolve(__dirname, './../src/ubold')],
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader'
        })
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {}
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg|ico)$/,
        loader: 'file-loader',
        options: {
          name: 'public/[name].[ext]?[hash]'
        }
      },
      {
        test: /\.(woff|woff2|otf|eot|ttf)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
      },
      {
        test: /\.pug$/,
        loader: 'pug-loader',
      },
    ]
  },
  plugins: [
    new ExtractTextPlugin('ubold.css'),
    new HtmlWebpackPlugin({
      chunks: ['main'],
      template: './src/index.pug',
      hash: true,
    }),
    new CopyWebpackPlugin([
        {from: 'src/ubold', to: 'public/ubold'},
        {from: 'src/styles/material-design-iconic-font', to: 'public/css/material-design-iconic-font'},
        {from: 'src/styles/fonts.css', to: 'public/css/fonts.css'},
        {from: 'src/fonts', to: 'public/fonts'},
        {from: 'src/images/favicon.ico', to: 'public/favicon.ico'},
        {from: 'src/images/500_.png', to: 'public/500_.png'},
    ]),
    new webpack.DefinePlugin({
      API_HOST: JSON.stringify(process.env.API_HOST || ''),
      NEWS_API_HOST: JSON.stringify(process.env.NEWS_API_HOST || '')
    }),
  ],
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: false
  },
  devtool: '#eval-source-map'
};
