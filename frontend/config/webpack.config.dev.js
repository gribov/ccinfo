const commonConfig = require('./webpack.config.common.js');
const merge = require('webpack-merge');

const devConfig = {
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true,
    port: 8080
  }
};

module.exports = merge(commonConfig, devConfig);
