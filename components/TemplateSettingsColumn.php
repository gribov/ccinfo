<?php
namespace app\components;

use app\models\TemplateSettings;
use Yii;
use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\ActionColumn;

/**
 * MaterialActionColumn extends after ActionColumn because of glyphicon and bootstrap 4.
 */
class TemplateSettingsColumn extends DataColumn
{
    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    public function getDataCellValue($model, $key, $index)
    {
        if (!$model instanceof TemplateSettings) {
            throw new \BadMethodCallException();
        }

        $this->format = 'raw';

        switch ($model->code) {
            case 'background':
                return $this->getValueForBackgroundSettings($model);
        }
        return $model->value;
    }

    /**
     * @param TemplateSettings $model
     * @return string
     */
    protected function getValueForBackgroundSettings(TemplateSettings $model)
    {
        $data = $model->encodedValue;

        if(($data['mode'] ?? null) === 'image') {
            $value = "<img height='80px' src='{$data['url']}'>";
            $value.= '<span style="margin-left: 10px;">Изображение ';
            if (isset($data['repeat'])) {
                $value.= $data['repeat'] ? '(замостить)' : '(растянутянуть)';
            }
            $value.='</span>';
        } else {
            $value = '<div  style="border: 1px solid; border-radius: 20px; width: 30px; height:30px; background: ' . $data['color'] . '"></div>';
        }

        return $value;
    }
}
