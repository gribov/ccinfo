# CC-info.

> Репозиторий проекта CC INFO

##### Описание :
 - Запрос на корень домена / обрабатывается выдачей frontend/index.html.
 - Запросы по роутам /(api|admin|auth)/* перенаправляются на Yii2.
 - Авторизация в админке через SSO.
 - API для приложения на Vue JS.
 - Приложения на Vue JS.
 - Модуль администратора.
 - Деплой через дженкинс.
 
 
##### Как разрабатывать фронтенд :

```
cd ./frontend
npm install
```
Запуск сервера разработки на `localhost:8080`
```
cd ./frontend
npm run dev
```
Сборка фронтенда с публикацией в папку `web/frontend`
```
cd ./frontend
npm run build
```

![alt text](./cc.gif?raw=true)

![alt text](./preview.png?raw=true)
