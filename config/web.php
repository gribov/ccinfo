<?php

$params = require(__DIR__ . '/params.php');
$env = require(__DIR__ . '/env.php');

$params = array_merge($params, $env ? $env['params'] : []);

$config = [
    'id' => 'CC info application',
    'name' => 'CC Info',
    'sourceLanguage'=>'en',
    'language'=>'ru-RU',
    'charset'=>'utf-8',
    'aliases' => [
        '@bower' => '@vendor/bower-asset'
    ],
    'homeUrl' => '/admin',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'ssoauth',
        'token',
        'api-auth',
        'slider'
    ],
    'modules' => [
        'admin-rbac' => [
            'class' => 'mdm\admin\Module',
            'layout' => '@app/modules/admin/views/layouts/admin.php',
        ],
        'ssoauth' => [
            'class' => 'phprad\ssoauth\SsoAuthModule',
            'appName' => $params['sso_auth']['appName'],
            'appKey' => $params['sso_auth']['appKey'],
            'authServerHost' => $params['sso_auth']['authServerHost'],
            'strategies' => [
                'redirect' => 'phprad\api\components\RedirectAfterSsoLoginStrategy'
            ],
        ],
        'token' => [
            'class' => 'phprad\accessToken\TokenModule',
            'userClass' => 'app\models\User',
            'includeRoutes' => ['token', 'profile'],
            'corsAllowOrigins' => [
                'http://localhost:8080',
            ],
            'apiVersion' => 'v1',
        ],
        'api-auth' => [
            'class' => 'phprad\api\ApiModule',
            'apiVersion' => 'v1',
            'allowedApps' => [
                ['host' => 'localhost', 'app_name' => 'my_local_app', 'app_key' => 'my_local_app_key'],
                ['host' => 'test-ccinfo.intranet', 'app_name' => 'my_local_app', 'app_key' => 'my_local_app_key'],
                ['host' => 'pp-ccinfo.intranet', 'app_name' => 'my_local_app', 'app_key' => 'my_local_app_key'],
                ['host' => 'ccinfo.intranet', 'app_name' => 'my_local_app', 'app_key' => 'my_local_app_key'],
            ]
        ],
        'api' => [
            'class' => 'app\modules\api\ApiModule',
        ],
        'admin' => [
            'class' => 'app\modules\admin\AdminModule',
        ],
        'nested_sets' => [
            'class' => 'app\modules\nested_sets\Module',
        ],
        'slider' => [
            'class' => 'app\modules\slider\Module',
        ],
    ],
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@mdm/admin/views' => '@app/modules/admin/views/admin',
                ]
            ]
        ],
        'request' => [
            'cookieValidationKey' => md5('AS34646ujYUJ*&'),
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/auth/ssoauth'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                ],
            ],
        ],
        'db' => $env ? $env['db']['db'] : require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/routes.php')
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'
                    ]
                ],
            ],
            'forceCopy' => YII_ENV_DEV
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'gii/*',
            'admin/admin/index',
            'admin/admin/test-login',
            'admin/admin/login',
            'admin/admin/logout',
            'admin/admin/logout',
            'ssoauth/auth/*',
            'api-auth/auth/*',
            'token/profile/*',
            'token/token/*',
            'api/category/*',
            'api/menu/*',
            'api/widgets/*',
            'api/page/*',
            'api/user-saved-link/*',
            'api/responsible-user/*',
            'api/default/ver',
            'api/default/404',
            'api/search/*',
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*']
    ];
}

return $config;
