<?php

/**
 * @return array|bool
 */
if (!function_exists('getEnvironment')) {
    function getEnvironment()
    {
        static $data;
        if ($data) {
            return $data;
        }

        $ini_path = __DIR__."/env.ini";

        if(!file_exists($ini_path)) {
            return false;
        }
        $data = parse_ini_file($ini_path, true);

        return $data;
    }
}

return getEnvironment();