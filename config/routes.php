<?php
return [
    ['class' => 'yii\rest\UrlRule', 'controller' => ['api/v1/user-saved-link' => 'api/user-saved-link']],
    ['class' => 'yii\rest\UrlRule', 'controller' => ['api/v1/responsible-user' => 'api/responsible-user']],
    // rules of token module will be here after init
    ['pattern' => 'api/v1/ver', 'route' => 'api/default/ver'],
    ['pattern' => 'api/v1/404', 'route' => 'api/default/404'],
    [
        'pattern' => '/api/v1/<controller>/<action>/<slug>',
        'route' => '/api/<controller>/<action>'
    ],
    [
        'pattern' => 'api/v1/<controller>/<action>',
        'route' => 'api/<controller>/<action>'
    ],
    [
        'pattern' => 'api/v1/<controller>',
        'route' => 'api/<controller>/index'
    ],
    [
        'pattern' => '/admin/category-item/<action>',
        'route' => '/nested_sets/category-item/<action>',
        'defaults' => ['action' => 'index'],
    ],
    [
        'pattern' => '/admin/navigation-menu/<action>',
        'route' => '/nested_sets/navigation-menu/<action>',
        'defaults' => ['action' => 'index'],
    ],
    [
        'pattern' => '/admin/widgets/slider/<action>',
        'route' => '/slider/slider/<action>',
        'defaults' => ['action' => 'index'],
    ],
    [
        'pattern' => '/admin/category-root/<action>',
        'route' => '/nested_sets/category-root/<action>',
        'defaults' => ['action' => 'index'],
    ],
    [
        'pattern' => '/admin/<action>',
        'route' => 'admin/admin/<action>',
        'defaults' => ['action' => 'index'],
    ]
];