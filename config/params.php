<?php

return [
    'adminEmail' => 'admin@example.com',
    'app_version' => '1.0',
    'sso_auth' => [
        'appName' => 'ccinfo_app',
        'appKey' => 'ccinfo_secret_key',
        'authServerHost' => 'api.intranet',
    ]
];
