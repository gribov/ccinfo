<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;dbname=starter',
    'username' => 'postgres',
    'password' => '',
    'charset' => 'utf8',
];