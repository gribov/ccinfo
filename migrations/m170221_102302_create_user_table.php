<?php

use yii\db\Migration;

class m170221_102302_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey()->unsigned(),
            'username' => $this->string()->notNull()->unique(),
            'name' => $this->string(),
            'email' => $this->string(50)->unique(),
            'theme' => $this->string(15),
            'status' => $this->boolean()->defaultValue(false),
            'created' => $this->timestamp(6)->defaultExpression('NOW()'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
