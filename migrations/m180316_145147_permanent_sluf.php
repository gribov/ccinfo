<?php

use yii\db\Migration;

/**
 * Class m180316_145147_permanent_sluf
 */
class m180316_145147_permanent_sluf extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('navigation_menu', 'permanent_slug', $this->string(500)->after('slug'));
        $this->addColumn('navigation_category', 'permanent_slug', $this->string(500)->after('slug'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('navigation_menu', 'permanent_slug');
        $this->dropColumn('navigation_category', 'permanent_slug');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180316_145147_permanent_sluf cannot be reverted.\n";

        return false;
    }
    */
}
