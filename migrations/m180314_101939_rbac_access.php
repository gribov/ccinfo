<?php

use yii\db\Migration;

/**
 * Class m180314_101939_rbac_access
 */
class m180314_101939_rbac_access extends Migration
{

    const ROUTES = [
        '/slider/slider/*',
        '/nested_sets/navigation-menu/*',
        '/nested_sets/category-item/*',
        '/nested_sets/category-root/*',
        '/admin/*',
        '/*',
    ];

    const EDITOR_ROUTES = [
        '/slider/slider/*',
        '/nested_sets/navigation-menu/*',
        '/nested_sets/category-item/*',
        '/nested_sets/category-root/*',
        '/admin/*',
    ];



    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $editor = $auth->getRole('operator');
        $editor->name = 'editor';
        $auth->update('operator', $editor);

        foreach (self::ROUTES as $route) {
            $_route = $auth->createPermission($route);
            $auth->add($_route);
        }

        $permission = $auth->getPermission('/*');
        $admin = $auth->getRole('admin');
        $auth->addChild($admin, $permission);

        foreach (self::EDITOR_ROUTES as $route) {
            $permission = $auth->getPermission($route);
            $editor = $auth->getRole('editor');
            $auth->addChild($editor, $permission);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $editor = $auth->getRole('editor');
        $editor->name = 'operator';
        $auth->update('editor', $editor);

        $permission = $auth->getPermission('/*');
        $admin = $auth->getRole('admin');
        $auth->removeChild($admin, $permission);

        foreach (self::EDITOR_ROUTES as $route) {
            $permission = $auth->getPermission($route);
            $editor = $auth->getRole('operator');
            $auth->removeChild($editor, $permission);
        }

        foreach (self::ROUTES as $route) {
            $_route = $auth->getPermission($route);
            $auth->remove($_route);
        }

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180314_101939_rbac_access cannot be reverted.\n";

        return false;
    }
    */
}
