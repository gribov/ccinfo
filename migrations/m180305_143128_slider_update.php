<?php

use yii\db\Migration;

/**
 * Class m180305_143128_slider
 */
class m180305_143128_slider_update extends Migration
{
    /** table prefix */
    const TABLE_PREFIX = 'phprad';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_PREFIX . '_slider_image', 'link', $this->string(1000));
        $this->addColumn(self::TABLE_PREFIX . '_slider_image', 'link_text', $this->string(1000));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_PREFIX . '_slider_image', 'link');
        $this->dropColumn(self::TABLE_PREFIX . '_slider_image', 'link_text');
    }
}
