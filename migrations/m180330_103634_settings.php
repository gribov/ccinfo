<?php

use yii\db\Migration;

/**
 * Class m180330_103634_settings
 */
class m180330_103634_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('template_settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(500),
            'code' => $this->string(50),
            'value' => $this->string(),
            'user_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-template_settings-user',
            'template_settings',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('template_settings');
    }
}
