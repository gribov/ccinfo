<?php

use yii\db\Migration;

/**
 * Class m180322_140608_saved_link_length
 */
class m180322_140608_saved_link_length extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('user_saved_links', 'title', $this->string(1000));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('user_saved_links', 'title', $this->string(100));
    }
}
