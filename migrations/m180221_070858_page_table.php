<?php

use yii\db\Migration;

/**
 * Class m180221_070858_page_table
 */
class m180221_070858_page_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'active' => $this->integer(1)->defaultValue(0),
            'title' => $this->string(500),
            'content' => $this->text(),
            'root_category_id' => $this->integer(),
            'category_id' => $this->integer(),
            'slug' => $this->string(550),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('pages');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180221_070858_page_table cannot be reverted.\n";

        return false;
    }
    */
}
