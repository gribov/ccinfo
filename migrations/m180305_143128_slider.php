<?php

use yii\db\Migration;

/**
 * Class m180305_143128_slider
 */
class m180305_143128_slider extends Migration
{
    /** table prefix */
    const TABLE_PREFIX = 'phprad';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_PREFIX . '_slider', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'code' => $this->string(50)->notNull()->unique(),
            'type' => $this->integer()->defaultValue(1)
        ]);

        $this->createTable(self::TABLE_PREFIX . '_slider_image', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
            'file_name' => $this->string(100)->notNull(),
            'slider_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-slide_image-slider_id',
            self::TABLE_PREFIX . '_slider_image',
            'slider_id',
            self::TABLE_PREFIX . '_slider',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_PREFIX . '_slider_image');
        $this->dropTable(self::TABLE_PREFIX . '_slider');
    }
}
