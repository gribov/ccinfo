<?php

use yii\db\Migration;

/**
 * Class m180316_090958_user_saved_links
 */
class m180316_090958_user_saved_links extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user_saved_links', [
            'id' => $this->primaryKey(),
            'route' => $this->string(100)->notNull(),
            'title' => $this->string(100)->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-slide_image-slider_id',
            'user_saved_links',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $role = new \mdm\admin\models\AuthItem([
            'name' => 'responsible',
            'type' => 1,
            'description' => 'Пользователь с этой ролью может быть назначен как ответственный.'
        ]);

        $role->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_saved_links');
        $role = \mdm\admin\models\AuthItem::findOne(['name' => 'responsible']);
        $role->remove();
    }
}
