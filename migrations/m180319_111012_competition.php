<?php

use yii\db\Migration;

/**
 * Class m180319_111012_competition
 */
class m180319_111012_competition extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('competition', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'slug' => $this->string(50)->notNull()->unique(),
            'position' => $this->string(50),
            'is_active' => $this->boolean()->defaultValue(false)
        ]);

        $this->createTable('competition_participant', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'photo_source' => $this->string(100)->notNull(),
            'competition_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-participant-competition_id',
            'competition_participant',
            'competition_id',
            'competition',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('competition_participant');
        $this->dropTable('competition');
    }
}
