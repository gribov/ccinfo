<?php

use yii\db\Migration;

/**
 * Class m180330_103634_settings
 */
class m180330_103634_create_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $settings = new \app\models\TemplateSettings([
            'name' => 'Задний фон сайта',
            'code' => 'background',
            'value' => 'red',
        ]);
        $settings->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\TemplateSettings::findOne(['code' => 'background'])->delete();
    }
}
