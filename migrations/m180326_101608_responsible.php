<?php

use yii\db\Migration;

/**
 * Class m180326_101608_responsible
 */
class m180326_101608_responsible extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('responsible_user', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'from' => $this->dateTime(),
            'to' => $this->dateTime(),
            'phone' => $this->string(10)
        ]);

        $this->addForeignKey(
            'fk-responsible_user-user',
            'responsible_user',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('responsible_user');
    }
}
