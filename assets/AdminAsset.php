<?php

namespace app\assets;

use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot/assets-admin';
    public $baseUrl = '@web/assets-admin';

    public $css = [
        'css/select2.min.css',
        'css/c3.min.css',
        'css/chartist.min.css',
        'css/chartist-init.min.css',
        'css/chartist-plugin-tooltip.min.css',
        'css/theme.style.min.css',
        'fonts/fonts.css',
        'css/blue.css',
        'css/style.css',
        'css/animate.css',
        'css/spinners.css',
        'icons/font-awesome/css/font-awesome.min.css',
        'icons/simple-line-icons/css/simple-line-icons.css',
        'icons/weather-icons/css/weather-icons.min.css',
        'icons/linea-icons/linea.css',
        'icons/themify-icons/themify-icons.css',
        'icons/flag-icon-css/flag-icon.min.css',
        'icons/material-design-iconic-font/css/materialdesignicons.min.css',
    ];

    public $js = [
        'js/jquery.slimscroll.js',
        'js/select2/select2.full.min.js',
        'js/waves.js',
        'js/sidebarmenu.js',
        'js/sticky-kit.min.js',
        'js/jquery.sparkline.min.js',
        'js/material_custom.js',
        'js/dashboard6.js',
        'js/jQuery.style.switcher.js',
        'js/custom.js',
    ];

    public $depends = [
        'app\assets\Bootstrap4Asset'
    ];
}
