<?php

namespace app\assets;

use yii\web\AssetBundle;

class IEAsset extends AssetBundle
{
    public $basePath = '@webroot/admin-assets';
    public $baseUrl = '@web/admin-assets';

    public $css = [];

    public $jsOptions = [
        'condition' => 'lt IE 9',
        'position' => \yii\web\View::POS_HEAD,
    ];

    public $js = [
        'js/html5shiv.js',
        'js/respond.min.js',
    ];

    public $depends = [];
}
