<?php

namespace app\assets;

use yii\web\AssetBundle;

class Bootstrap4Asset extends AssetBundle
{
    public $basePath = '@webroot/assets-admin';
    public $baseUrl = '@web/assets-admin';

    public $css = [
        'css/bootstrap.min.css',
    ];

    public $js = [
        'js/popper.min.js',
        'js/bootstrap.min.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
