$(document).ready(function () {

    /**
     * Инциализация select2 с автодополнением по ajax
     * @param id
     * @param url
     * @param placeholder
     * @param filter
     */
    function initSelect2(id, url, placeholder, filter) {

        filter = filter || false;

        var select = $(id);

        select.select2({
            placeholder: placeholder,
            allowClear: true,
            minimumInputLength: 1,
            ajax: {
                url: url,
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term,
                        filter: filter
                    };
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: function (data) {
                return data.text;
            },
            templateSelection: function (data) {
                return data.text;
            }
        });

        /**
         * Сброс для форм yii2
         */
        select.on('select2:unselecting', function () {
            select.empty().trigger('change');
            select.html('<option value>' + placeholder + '</option>');
        });
    }

    /**
     * Главное меню, для заполнения формы
     */
    initSelect2('#navigationmenu-category_id', '/admin/ajax/autocomplete-category', 'Начните вводить название категории');
    initSelect2('#navigationmenu-page_id', '/admin/ajax/autocomplete-page', 'Начните вводить название страницы');

    /**
     * Фильтрация главного меню
     */
    initSelect2('#filter-category-name', '/admin/ajax/autocomplete-category', 'Начните вводить название категории', true);
    initSelect2('#filter-page-title', '/admin/ajax/autocomplete-page', 'Начните вводить название страницы', true);

});