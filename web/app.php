<?php
$env = require(__DIR__ . '/../config/env.php');

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', ($env ? (bool) $env['DEBUG'] : true));
defined('YII_ENV') or define('YII_ENV', ($env ? $env['ENV'] : 'dev'));

if(YII_DEBUG) {
    function p($data)
    {
        echo '<pre>';
        var_dump($data);
        echo '</pre>';
    }
}

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
