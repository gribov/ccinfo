<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "competition".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $position
 * @property bool $is_active
 *
 * @property CompetitionParticipant[] $competitionParticipants
 */
class Competition extends \yii\db\ActiveRecord
{
    /**
     * home page
     */
    const POSITION_HOME = 'home';
    const POSITION_HOME_NAME = 'Главная';

    const POSITIONS = [
        self::POSITION_HOME => self::POSITION_HOME_NAME
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'competition';
    }

    /**
     * @return array
     */
    public static function getPositions()
    {
        return self::POSITIONS;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['slug', 'required', 'on' => 'update'],
            [['is_active'], 'boolean'],
            ['is_active', 'default', 'value' => true],
            [['name'], 'string', 'max' => 100],
            [['slug', 'position'], 'string', 'max' => 50],
            [['slug'], 'unique'],
            ['position', 'validatePosition', 'when' => function ($model) {
                return $model->position;
            }],
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->getIsNewRecord()) {
            $this->scenario = 'update';
        }
        return parent::beforeValidate();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название конкурса',
            'slug' => 'Slug',
            'is_active' => 'Активность',
            'position' => 'Страница отображения',
            'positionName' => 'Страница отображения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionParticipants()
    {
        return $this->hasMany(CompetitionParticipant::className(), ['competition_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function canAddParticipant()
    {
        return count($this->competitionParticipants) < 3;
    }

    /**
     * @param $attribute
     */
    public function validatePosition($attribute)
    {
        $model = self::findOne(['position' => self::POSITION_HOME]);

        if ($model && $model->id !== $this->id) {
            $this->addError($attribute, 'Позиция занята');
        }
    }

    /**
     * @return mixed|string
     */
    public function getPositionName()
    {
        return self::POSITIONS[$this->position] ?? 'нет';
    }
}
