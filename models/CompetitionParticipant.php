<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "competition_participant".
 *
 * @property int $id
 * @property string $name
 * @property string $photo_source
 * @property int $competition_id
 *
 * @property Competition $competition
 */
class CompetitionParticipant extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile
     */
    public $photoFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'competition_participant';
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'name',
            'img' => function ($model) {
                return Url::base(true) . $model->photoSrc;
            }
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['photo_source'], 'required'],
            [['photoFile'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg', 'jpeg'], 'checkExtensionByMimeType' => false],
            [['competition_id'], 'default', 'value' => null],
            [['competition_id'], 'integer'],
            [['name', 'photo_source'], 'string', 'max' => 100],
            [['competition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Competition::className(), 'targetAttribute' => ['competition_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'photo_source' => 'Фото',
            'competition_id' => 'Конкурс',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetition()
    {
        return $this->hasOne(Competition::className(), ['id' => 'competition_id']);
    }

    /**
     * @return bool
     */
    public function upload()
    {
        $this->photo_source = 'competition_' . time() . $this->photoFile->baseName . '.' . $this->photoFile->extension;

        if ($this->validate()) {
            $this->photoFile->saveAs(Yii::getAlias('@webroot/assets-admin/competition/') . $this->photo_source);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getPhotoSrc()
    {
        return Yii::getAlias('@web/assets-admin/competition/') . $this->photo_source;
    }
}
