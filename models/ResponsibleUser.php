<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "responsible_user".
 *
 * @property int $id
 * @property int $user_id
 * @property string $from
 * @property string $to
 * @property string $phone
 *
 * @property User $user
 */
class ResponsibleUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'responsible_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'default', 'value' => null],
            [['user_id'], 'integer'],
            [['from', 'to', 'phone'], 'required'],
            //[['from', 'to'], 'safe'],
            [['from', 'to'], 'date', 'format' => 'yyyy-M-d H:m:s'],
            [['from', 'to'], 'validateFromTo'],
            [['phone'], 'string', 'max' => 10],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'from' => function($model) {
                return (new \DateTime($model->from))->format(\DateTime::ISO8601);
            },
            'to' => function($model) {
                return (new \DateTime($model->to))->format(\DateTime::ISO8601);
            },
            'phone',
            'user' => function ($model) {
                return $model->user->name;
            },
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'Время с',
            'to' => 'Время по',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param $attribute
     */
    public function validateFromTo($attribute)
    {
        if (isset($this->errors['from']) || isset($this->errors['to'])) {
            return;
        }

        if (new \DateTime($this->from) >= new \DateTime($this->to)) {
            $this->addError($attribute, 'Значние атрибута ' . $attribute . ' должно быть больше чем ' . $this->from);
        }
    }
}
