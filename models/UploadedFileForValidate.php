<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadedFileForValidate extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @var UploadedFile
     */
    public $anotherFile;

    const SCENARIO_IMAGE = 'image';
    const SCENARIO_FILE = 'file';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_IMAGE] = ['imageFile'];
        $scenarios[self::SCENARIO_FILE] = ['anotherFile'];
        return $scenarios;
    }

    public function rules()
    {
        return [
            [['imageFile'], 'file',
                'skipOnEmpty' => false,
                'minSize' => 10,
                'checkExtensionByMimeType' => false,
                //'mimeTypes' => ['image/jpeg', 'image/pjpeg', 'image/png'],
                'extensions' => ['pjpeg', 'jpeg', 'jpg', 'png'],
                'on' => self::SCENARIO_IMAGE
            ],

            [['anotherFile'], 'file',
                'skipOnEmpty' => false,
                'minSize' => 10,
                'checkExtensionByMimeType' => false,
                //'mimeTypes' => ['application/vnd.ms-excel', 'application/pdf', 'application/msword', 'application/rtf', 'application/x-rar-compressed', 'application/zip', 'application/x-7z-compressed', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'],
                'extensions' => ['xsl', 'pdf', 'doc', 'rtf', 'rar', 'zip', '7z', 'docx', 'xlsx', 'pptx', ],
                'on' => self::SCENARIO_FILE
            ],
        ];
    }

}