<?php

namespace app\models;

use app\models\auth\AuthAssignment;
use phprad\accessToken\interfaces\AuthByAccessTokenTokenInterface;
use phprad\accessToken\traits\AuthByAccessTokenTokenTrait;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface, AuthByAccessTokenTokenInterface
{
    /** trait */
    use AuthByAccessTokenTokenTrait;

    /** @var string  */
    public $defaultRole = 'user';

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'user';
    }
    /**
     * @return array
     */
    public function fields()
    {
        return [
            'username',
            'name',
            'email',
            'roles' => function ($model) {
                return array_map(function($item) {
                    return $item->item_name;
                }, $model->roles);
            }
        ];
    }


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['created'], 'safe'],
            [['username'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 100],
            [['email'], 'string', 'max' => 50],
            [['theme'], 'string', 'max' => 15],
            [['status'], 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'created' => 'Created',
            'name' => 'Имя',
            'email' => 'Email',
            'theme' => 'Theme',
            'status' => 'Статус'
        ];
    }

    /**
     * @param $auth_api
     * @return User|null|static
     */
    public static function checkUser($auth_api)
    {
        die;
        $user = self::findByUsername($auth_api->data->samaccountname); 

        if (empty($user))
            $user = new self;
        
        $user->username = $auth_api->data->samaccountname;
        $user->email = $auth_api->data->mail ?? NULL;
        $user->name = $auth_api->data->displayname ?? NULL;
        $user->save();

        if (empty(AuthAssignment::findOne(['user_id' => $user->id]))) {
            $aa = new AuthAssignment;
            $aa->item_name = $user->deafultRole;
            $aa->user_id = (string)$user->id;
            $aa->save();
        }

        return $user;
    }

    /**
     * @param int|string $id
     * @return null|static
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @param $username
     * @return null|static
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username'=>$username]);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    public function getRolesAsString()
    {
        return join(' ', array_map(function(AuthAssignment $role) {
            return $role->item_name;
        }, $this->roles));
    }

}
