<?php
/**
 * Created by PhpStorm.
 * User: er22318
 * Date: 03.04.2018
 * Time: 16:56
 */

namespace app\models;


use yii\web\UploadedFile;

class TemplateBackgroundForm extends \yii\base\Model
{
    const TYPE_FILL = 1;
    const TYPE_IMAGE = 2;

    const TYPES = [
        self::TYPE_FILL,
        self::TYPE_IMAGE,
    ];

    const SCENARIO_FILL_BACK = 'fill_background';

    const SCENARIO_IMAGE_BACK = 'image_background';

    /** @var  int */
    public $type;

    /** @var  UploadedFile */
    public $imageFile;

    /** @var  bool repeat_image */
    public $repeatImage;

    /** @var  string */
    public $color;

    /** @var  string */
    public $imageFileName;

    /** @var  boolean */
    public $imageFileAttached;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => self::TYPES],
            ['color', 'required', 'on' => self::SCENARIO_FILL_BACK],
            ['repeatImage', 'boolean'],
            [['color', 'imageFileName'], 'string'],
            [['imageFile'], 'file',
                'skipOnEmpty' => true,
                'minSize' => 10,
                'checkExtensionByMimeType' => false,
                'extensions' => ['pjpeg', 'jpeg', 'jpg', 'png'],
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getBackgroundTypes()
    {
        return [
            self::TYPE_FILL => 'Заливка одним цветом',
            self::TYPE_IMAGE => 'Изображение',
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'color' => 'Цвет фона',
            'repeatImage' => 'Повторять изображение',
            'imageFile' => 'Файл',
            'type' => 'Тип фона',
        ];
    }

    /**
     * @return bool
     */
    public function identifyScenario()
    {
        if ($this->type === self::TYPE_FILL) {
            $this->scenario = self::SCENARIO_FILL_BACK;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function saveImage()
    {
        if ($this->imageFile && (int)$this->type === self::TYPE_IMAGE) {
            $name = time()
                . '.'
                . $this->imageFile->extension;

            if ($this->imageFile->saveAs(\Yii::getAlias('@webroot/images/background_images/') . $name)) {
                $this->imageFileName = $name;
                return true;
            } else {
                $this->addError('imageFile', 'Ошибка сохраниения файла');
                return false;
            }
        }
        return true;
    }

    /**
     * @param TemplateSettings $entity
     * @return bool
     */
    public function saveEntity(TemplateSettings $entity)
    {
        $mode = intval($this->type) === self::TYPE_IMAGE ? 'image' : 'fill';

        $settings = [
            'mode' => $mode,
        ];

        if (intval($this->type) === self::TYPE_IMAGE) {
            $settings['url'] = \Yii::getAlias('@web/images/background_images/') . $this->imageFileName;
            $settings['repeat'] = $this->repeatImage;
        }

        if (intval($this->type) === self::TYPE_FILL) {
            $settings['color'] = $this->color;
        }

        $entity->value = json_encode($settings);

        $entity->user_id = \Yii::$app->user->identity->getId();

        return $entity->save();
    }

    /**
     * @param TemplateSettings $entity
     */
    public function loadEntity(TemplateSettings $entity)
    {
        $values = @json_decode($entity->value ?? '{}', true);
        if (!$values) {
            return;
        }
        $this->type = (($values['mode'] ?? null) === 'image') ? self::TYPE_IMAGE : self::TYPE_FILL;
        $this->repeatImage = (bool)($values['repeat'] ?? null);

        $this->color = $values['color'] ?? null;

        if ($values['url'] ?? false) {
            $parts = explode('/', $values['url']);
            $this->imageFileName = array_pop($parts);
            $this->imageFileAttached = true;
        }
    }

    /**
     * @return string
     */
    public function getImageFileSrc()
    {
        return \Yii::getAlias('@web/images/background_images/') . $this->imageFileName;
    }

}