<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "template_settings".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $value
 * @property int $user_id
 *
 * @property User $user
 */
class TemplateSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'template_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'default', 'value' => null],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 500],
            [['code'], 'string', 'max' => 50],
            [['value'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя свойства',
            'code' => 'Код свойства',
            'value' => 'Значение свойства',
            'user_id' => 'Изменено пользователем',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return array|mixed
     */
    public function getEncodedValue()
    {
        $json = @json_decode($this->value, true);

        return $json ? $json : [];
    }

    /**
     * @return null|static
     */
    public static function getTemplateSettingsBackground()
    {
        $background = self::findOne(['code' => 'background']);
        $background = @json_decode($background->value, true);

        if ($background['url'] ?? false) {
            $background['url'] = 'http://' . \Yii::$app->request->serverName . $background['url'];
        }

        return $background;
    }
}
