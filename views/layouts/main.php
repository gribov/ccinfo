<?php

use yii\helpers\Html;

/**
 * @var $content string
 */
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
    </head>

    <body class="widescreen">
    <?php $this->beginBody() ?>

    <div id="wrapper">
            <div class="content">
                <div class="container">
                    <?= $content ?>
                </div>
            </div>
    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>